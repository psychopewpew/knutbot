#!/usr/bin/env python3

import irc.bot
from irc.client import MessageTooLong
import argparse
import time
import threading
import logging
import migrations
import importlib
from logging.handlers import RotatingFileHandler, TimedRotatingFileHandler
from common import permissions, config, utils, usertracker
from modules import controller
from common.knutbotapi import api

logformatter = logging.Formatter('[%(asctime)s] %(message)s')


class Knutbot(irc.bot.SingleServerIRCBot):

    def __init__(self):

        self.token = config.get('token')
        self.channel = config.get('channels') \
            if config.runtype == config.moduletype["remote"] \
            else [config.get('localchannel')]

        utils.ensure_dir_exists("logs")
        logger = logging.getLogger("general")
        h = TimedRotatingFileHandler("logs/general.log", when="W0", backupCount=3)
        h.setFormatter(logformatter)
        logger.addHandler(h)
        logger.setLevel(logging.INFO)

        # Apply migrations
        migration_level = config.get_default(0, 'migration_level')
        available_migrations = migrations.migration_order
        if migration_level < len(available_migrations):
            required_migrations = available_migrations[migration_level:]
            for migration in required_migrations:
                print("Applying migration {}".format(migration))
                migration_module = importlib.import_module('migrations.' + migration)
                migration_module.migrate()
            config.set(len(available_migrations), 'migration_level')
            
        # Create IRC bot connection
        server = 'irc.chat.twitch.tv'
        port = 6667
        print ('Connecting to ' + server + ' on port ' + str(port) + '...')
        self.username = config.get('username')
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port, self.token)],
            self.username, self.username)

    def collect_time_messages(self):
        if self.connection.is_connected():
            for channel in self.channel:
                msgs = controller.collect_time_messages(channel)
                self.send_msg(self.connection, channel, msgs)

    def _collect_time_messages_loop(self):
        while not config.exit_event.is_set():
            config.timedmsg_event.clear()
            self.collect_time_messages()
            config.timedmsg_event.wait()

    def on_dccchat(self, connection, event):
        print(connection, event)

    def on_welcome(self, c, e):
        # You must request specific capabilities before you can use them
        c.cap('REQ', ':twitch.tv/membership')
        c.cap('REQ', ':twitch.tv/tags')
        c.cap('REQ', ':twitch.tv/commands')
        self.caps = 3

    def on_cap(self, c, e):
        self.caps -= 1
        if self.caps == 0:
            # We have all capabilities
            for channel in self.channel:
                print('Joining ' + channel)
                c.join(channel)
                controller.init_channel(channel)
                utracker = usertracker.gettracker(channel)

                if not config.has('modules',channel):
                    config.add('moduleloader', 'modules', channel)
                # Check if usertracker knows owner
                if not utracker.hasowner():
                    utracker.update_permission(channel[1:], permissions.OWNER)
                elif utracker.get(channel[1:]).permission < permissions.OWNER:
                    utracker.update_permission(channel[1:], permissions.OWNER)

                for module in config.get('modules', channel):
                    msg = controller.activate_module(module, channel, surpress_msg=True)
                    self.send_msg(c, channel, msg)

            if config.has("permission"):
                config.delete("permission")
            threading.Thread(target=self._collect_time_messages_loop).start()
            if config.runtype == config.moduletype["remote"]:
                threading.Thread(target=api.startServer, daemon=True).start()
            

    def on_join(self, c, e):
        # print(e)
        pass

    def on_part(self, c, e):
        # print(e)
        pass

    def on_mode(self, c, e):
        # print(e,"\n")
        pass
        
    def on_usernotice(self, c, e):
        # print(e,"\n")
        pass
        
    def on_userstate(self, c, e):
        # print(e,"\n")
        pass
        
    def on_roomstate(self, c, e):
        # print(e,"\n")
        pass
        
    def on_notice(self, c, e):
        # print(e,"\n")
        pass
        
    def on_hosttarget(self, c, e):
        # print(e)
        pass

    def on_action(self, c, e):
        self.on_pubmsg(c,e)

    def on_pubmsg(self, c, e):
        # If a chat message starts with an exclamation point, try to run it as a command
        chatmsg = e.arguments[0].split(' ')
        target = e.target
        if target.startswith("#chatrooms"):
            start = 11  # start of id is after "#chatrooms:[ID]"
            end = target.find(':', start)
            channel = "#" + apihelper.get_username(target[start:end])
        else:
            channel = target
        username = e.source[:e.source.index('!')]
        if self.username == username:
            return
        user = usertracker.gettracker(channel).update_and_get(username,e.tags)
        is_command = False
        # when message is command
        if e.arguments[0].startswith(config.cmdprefix):
            command = chatmsg[0][len(config.cmdprefix):].lower()
            args = chatmsg[1:] if len(chatmsg) > 1 else []
            is_command = True
            if config.debug:
                logging.getLogger("general").info("PUBMSG: {} used command '{} {}' in {}".format(user, command, " ".join(args), channel))
            msg = controller.runcommand(command, user, channel, args)
            self.send_msg(c, target, msg)

        # run trigger on all messages but the bot's own
        msg = controller.runmessagetrigger(chatmsg, user, channel, is_command)
        self.send_msg(c, target, msg)

    def on_whisper(self, c, e):
        # Whispers by bot
        chatmsg = e.arguments[0].split(' ')
        username = e.source[:e.source.index('!')]
        if chatmsg[0].startswith('#'):
            channel = chatmsg[0]
            chatmsg[:] = chatmsg[1:]
        else:
            channel = "#" + username
        user = usertracker.gettracker(channel).update_and_get(username,e.tags)
        if channel in self.channel and chatmsg[0].startswith(config.cmdprefix):
            command = chatmsg[0][len(config.cmdprefix):].lower()
            args = chatmsg[1:] if len(chatmsg) > 1 else []
            if config.debug:
                logging.getLogger("general").info("WHISPER: {} used command '{} {}' in {}".format(user, command, " ".join(args), channel))
            msg = controller.runcommand(command, user, channel, args, whisper=True)
            self.send_whisper(c, e.target, username, msg)

    def on_namreply(self, c, e):
        #print(e)
        pass

    def send_msg(self, c, channel, messages):
        for m in messages:
            try:
                c.privmsg(channel, m)
            except MessageTooLong:
                c.privmsg(channel, "The message that was tried to sent was too long. Please contact your host for this bot")
                errorlogger = logging.getLogger("error")
                errorlogger.error("Error sending too long message. Original message:\n{}".format(m))
            time.sleep(0.2)

    def send_whisper(self, c, target, user, messages):
        for m in messages:
            c.send_items('PRIVMSG', target, ':.w', user, m)
            time.sleep(1)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("mode", help="Decide weather you want to run it in \
        local or remote mode\nLocal connects to your local channel and can \
        only run local modules.\nRemote connect to all channels but can only \
        run remote modules")
    parser.add_argument("-cs", "--commandstats", help="Should all used \
        commands in channels be logged into a database?", action="store_true")
    parser.add_argument("-d", "--debug", action="store_true")
    args = parser.parse_args()
    if args.mode != 'local' and args.mode != 'remote':
        parser.error('mode must be either local or remote')

    config.runtype = config.moduletype[args.mode]
    config.log_command_stats = args.commandstats
    config.debug = args.debug

    controller.init()
    bot = Knutbot()

    import signal
    for sig in ('TERM', 'INT'):
        signal.signal(getattr(signal, 'SIG'+sig), lambda s, f: die(bot))

    try:
        bot.start()
    except KeyboardInterrupt:
        die(bot)


def die(bot):
    config.exit_event.set()
    config.timedmsg_event.set()
    bot.die()


if __name__ == "__main__":
    main()
