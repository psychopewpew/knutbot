from . import controller
from common import utils, config, permissions
from common.decorator import Command

moduletype = config.moduletype["remote"] | config.moduletype['local']
triggertype = config.triggertype['command']

def _modules(channel, user, args):
    return "Available modules: {}".format(', '.join(controller._modules)), "Active modules: {}".format(", ".join(sorted(controller._activated_modules[channel])))

@Command(required_permission=permissions.OWNER)
def modules(channel, user, args):
    _usage = "Usage: {0}modules list | {0}modules activate|deactivate|help <module>".format(config.cmdprefix)
    if len(args) == 1 and args[0] == "list":
        return _modules(channel, user, args)
    if len(args) != 2:
        return _usage

    if args[0] == "activate":
        return controller.activate_module(args[1], channel)
    elif args[0] == "deactivate":
        return controller.deactivate_module(args[1], channel)
    elif args[0] == "help":
        if args[1] not in controller._modules:
            return "Module {} does not exist".format(args[1])
        return "Check the wiki for more informations: {}modules/{}".format(config.WIKI_URL, args[1])
    else:
        return _usage

@Command()
def commands(channel, user, args):
    if len(args) == 0:
        permobj = permissions.EVERYONE
    else:
        level = args[0].upper()
        try:
            permobj = getattr(permissions, level)
        except:
            return "{}commands (<permission level>) to get all commands available to the given permission level. If no permission level is given, all commands available to EVERYONE are listed".format(config.cmdprefix)
    commandlist = [c for c in controller._commands[channel].values()
        if c.required_permission == permobj]
    resultlist = sorted(map(lambda c: "{}{}".format(config.cmdprefix, c.__name__), commandlist))
    resultlist.insert(0, utils.at_user(user))
    return utils.compress_message_list(resultlist)
