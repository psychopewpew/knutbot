from common import config, utils, permissions, usertracker, genericdatabase, cooldown
from common.genericcounter import GenericCounter
from common.decorator import Command, CustomCommand
from common.twitchapi import helper as twitchhelper
from modules import controller

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']

database = {}

DATABASE_FILENAME = "res/numbers.db"


class CustomCounter(GenericCounter):

    def __init__(self, channel, name, verb):
        super().__init__(channel, name, verb)


def _add_custom_counter(channel, name, verb):
    counter = CustomCounter(channel, name, verb)
    database[channel][name] = counter
    commands = []
    commands.append(_create_add_command(channel, name))
    commands.append(_create_all_command(channel, name))
    commands.append(_create_clean_command(channel, name))
    commands.append(_create_get_command(channel, name))
    commands.append(_create_set_command(channel, name))
    counter.commands = commands
    return commands


def _del_custom_counter(channel, name):
    counterobj = database[channel][name]
    for cmd in counterobj.commands:
        del controller._commands[channel][cmd]
    del database[channel][name]


def _create_add_command(channel, name):
    command = "add"+name        
    @CustomCommand(command, call_if_on_cooldown=on_add_cooldown)
    def _custom(channel, user, args):
        if len(args) > 0:
            return database[channel][name].add(" ".join(args))
        else:
            return database[channel][name].add()

    utils.setup_custom_command(channel, _custom, command, __name__)
    return command


def _create_all_command(channel, name):
    command = "all"+name
    @CustomCommand(command)
    def _custom(channel, user, args):
        return database[channel][name].sum()

    utils.setup_custom_command(channel, _custom, command, __name__)
    return command


def _create_get_command(channel, name):
    command = name
    @CustomCommand(command)
    def _custom(channel, user, args):
        if len(args) > 0:
            return database[channel][name].current(" ".join(args))
        else:
            return database[channel][name].current()

    utils.setup_custom_command(channel, _custom, command, __name__)
    return command


def _create_clean_command(channel, name):
    command = "clear"+name
    @CustomCommand(command, required_permission=permissions.OWNER)
    def _custom(channel, user, args):
        return database[channel][name].clear()

    utils.setup_custom_command(channel, _custom, command, __name__)
    return command


def _create_set_command(channel, name):
    command = "set"+name
    @CustomCommand(command, required_permission=permissions.MOD)
    def _custom(channel, user, args):
        usage = "Usage: {}set{} <count> (<game name>)".format(config.cmdprefix, name)
        if len(args) == 0:
            return usage
        try:
            count = int(args[0])
        except:
            return usage
        if len(args) > 1:
            return database[channel][name].set(count, " ".join(args[1:]))
        else:
            return database[channel][name].set(count)

    utils.setup_custom_command(channel, _custom, command, __name__)
    return command


def init(channel):
    database[channel] = {}
    database[channel]["core"] = genericdatabase.GenericDatabase(
        DATABASE_FILENAME, channel[1:], [["name", "TEXT", "PRIMARY KEY"], 
        ["verb", "TEXT"]])
    entries = database[channel]["core"].getall()
    for entry in entries:
        _add_custom_counter(channel, *entry)


def notice_on_activate(channel):
    if not twitchhelper.valid():
        return "Please inform the host of this bot to set get an OAuth-Token for the Twitch API. Automatic current game detection does not work without it."


def on_deactivate(channel):
    entries = database[channel]["core"].getall()
    for entry in entries:
        _del_custom_counter(channel, entry[0])


def on_add_cooldown(channel, user, args):
    return utils.at_user(user) + " add ignored because it is on cooldown"


def getall(channel):
    try:
        return database[channel]["core"].getall()
    except KeyError:
        return None


@Command(required_permission=permissions.MOD)
def addcustomcounter(channel, user, args):
    if len(args) < 2:
        return "Usage: {}addcustomcounter <name/noun> <verb (past tense)>".format(config.cmdprefix)
    name = args[0]
    verb = " ".join(args[1:])
    if not database[channel]["core"].add((True, False), (name, verb)):
        return "Custom counter {} already exists".format(name)
    rawcommands = _add_custom_counter(channel, name, verb)
    commands = list(map(lambda c: "{}{}".format(config.cmdprefix, c), 
        rawcommands))
    # Add default cooldown on the add command to prevent users
    # adding the same "event" multiple times accidentally
    cd = {"cooldown": 5, "type": "global", "ignoremod": False}
    cooldown.cooldowns.set(cd, channel, rawcommands[0])
    return "Added custom counter. New commands: " + " ".join(commands), \
        "Set default cooldown to 5 seconds for {} (global, \
        mods don't ignore cooldown)".format(commands[0])


@Command(required_permission=permissions.MOD)
def delcustomcounter(channel, user, args):
    if len(args) != 1:
        return "Usage: {}delcustomcounter <name/noun>".format(config.cmdprefix)
    name = args[0]
    if database[channel]["core"].delete((True, False), (name, None)):
        _del_custom_counter(channel, name)
        return "Deleted the custom counter."
    else:
        return "The custom counter doesn't exist."

    
