import random
import sys
import os
import errno
from common import utils, config, permissions, pointtracker
from common.decorator import Command

moduletype = config.moduletype["local"] | config.moduletype["remote"]
triggertype = config.triggertype['command']

_instances = {}

_islocal = config.runtype == config.moduletype["local"]
_config_save_infos_path = ("marbles", "path_to_save_infos")

class Tracker:

    def __init__(self, channel):
        self.pointtracker = pointtracker.MonthlyPointTracker("res/marbles_{}.db".format(channel[1:]))
        self.channel = channel
        self.autoadd(config.moduleconfig.get_default(False, "marbles", self.channel, "autoadd"))
        if _islocal:
            if not config.moduleconfig.has(*_config_save_infos_path):
                config.moduleconfig.set("", *_config_save_infos_path)
            self.infosavepath = config.moduleconfig.get(*_config_save_infos_path)
            self.verifyInfosavepath()
            self.saveToTextIfLocal()

    def addPlayer(self, name):
        successful = self.pointtracker.addPlayer(name)
        if successful:
            return "Player added to the database."
        else:
            return "Player already exists!"

    def addPoints(self, pointlist):
        players = []
        for entry in pointlist:
            player = self.pointtracker.getPlayer(entry[0])
            if player == None:
                return "Player {} does not exist!".format(entry[0])
            players.append(player)
        result = []
        for entry, player in zip(pointlist, players):
            points = entry[1]
            result.append("({}: {} -> {})".format(*self.pointtracker.addPoints(player, points)))
        self.saveToTextIfLocal()
        return ", ".join(result)

    def getPoints(self, username):
        player = self.pointtracker.getPlayer(username)
        if player == None:
            return "Player {} does not exist!".format(username)
        if not self.pointtracker.currentTableExists():
            return "No games were played in the current month."
        standings = self.pointtracker.getStandings()
        sortstandings = sorted(standings, key=lambda x: x[1], reverse=True)
        rank = 0
        curpoints = sys.maxsize
        step = 1
        for place in sortstandings:
            if place[1] < curpoints:
                rank += step
                curpoints = place[1]
                step = 1
            else:
                step += 1
            if place[0].lower() == username.lower():
                return "{}: {} points (Rank: {})".format(place[0], place[1], rank)
        return "Player {} never played.".format(username)

    def getRank(self, rank):
        if not self.pointtracker.currentTableExists():
            return "No games were played in the current month."
        standings = self.pointtracker.getStandings()
        if len(standings) < rank:
            return "Rank {} does not exist (lowest rank is {})".format(rank, len(standings))
        sortstandings = sorted(standings, key=lambda x: x[1], reverse=True)
        currank = 0
        curpoints = sys.maxsize
        step = 1
        for place in sortstandings:
            if place[1] < curpoints:
                currank += step
                curpoints = place[1]
                step = 1
            else:
                step += 1
            if rank == currank:
                return "{}: {} points (Rank: {})".format(place[0], place[1], currank)
            elif currank+step > rank:
                return "{}: {} points (Rank: {}) -- There are {} players with rank {}".format(place[0], place[1], currank, step, currank)
        return "Rank not found, should never happen BibleThump"

    def getRigged(self):
        if not self.pointtracker.currentTableExists():
            return "No games were played in the current month."
        standings = self.pointtracker.getStandings()
        sortstandings = sorted(standings, key=lambda x: x[1], reverse=True)[:5]
        texts = []
        for place in sortstandings:
            texts.append("{}: {}".format(place[0], place[1]))
        return "Top 5 players: " + ", ".join(texts)


    def pickWinner(self, year, month):
        try:
            standings = self.pointtracker.getStandings({"year": year, "month": month})
        except:
            return "No games were played in {}-{}.".format(year, month)
        sortstandings = sorted(standings, key=lambda x: x[1], reverse=True)
        pointsum = sum([x[1] for x in sortstandings])
        winnernumber = random.randrange(0,pointsum)
        for place in sortstandings:
            winnernumber -= place[1]
            if winnernumber < 0:
                return "{} wins!".format(place[0])


    def renamePlayer(self, oldname, newname):
        if self.pointtracker.renamePlayer(oldname, newname):
            return "Changed the name from {} to {}".format(oldname, newname)
        else:
            return "Player {} does not exist or {} exists already!".format(oldname, newname)

    def saveToTextIfLocal(self):
        if _islocal:
            if not self.infosavepath:
                config.moduleconfig.load()
                self.infosavepath = config.moduleconfig.get(*_config_save_infos_path)
                self.verifyInfosavepath()
            if self.infosavepath:
                standings = self.pointtracker.getStandings()
                if standings == None:
                    # Erase file data if table doesnt exist - most likely month swapped
                    with open(self.infosavepath + "firstplace.txt", "w") as f:
                        f.write("\n")
                    with open(self.infosavepath + "top5.txt", "w") as f:
                        f.write("\n")
                    with open(self.infosavepath + "allstandings.txt", "w") as f:
                        f.write("\n")
                else:
                    sortstandings = sorted(standings, key=lambda x: x[1], reverse=True)
                    with open(self.infosavepath + "firstplace.txt", "w") as f:
                        f.write("{}: {}".format(sortstandings[0][0], sortstandings[0][1]))
                    with open(self.infosavepath + "top5.txt", "w") as f:
                        for entry in sortstandings[:5]:
                            f.write("{}: {}\n".format(entry[0], entry[1]))
                        f.write("\n")
                    with open(self.infosavepath + "allstandings.txt", "w") as f:
                        for entry in sortstandings:
                            f.write("{}: {}\n".format(entry[0], entry[1]))
                        f.write("\n")

    def verifyInfosavepath(self):
        if self.infosavepath and not self.infosavepath.endswith(os.sep):
            self.infosavepath += os.sep
            config.moduleconfig.set(self.infosavepath, *_config_save_infos_path)
        try:
            if self.infosavepath:
                os.makedirs(self.infosavepath)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

    def autoadd(self, b):
        if config.cmdprefix != "!":
            return "Autoadd is only supported when the command prefix is '!'."
        config.moduleconfig.set(b, "marbles", self.channel, "autoadd")
        from modules.controller import _commands
        if b:
            _commands[self.channel]["play"] = lambda c, u, a: utils.wrap_and_change_color(c,
                "Player {} added to the marbles database".format(u)
                if self.pointtracker.addPlayer(str(u)) else None)
            return "Auto adding activated"
        else:
            if "play" in _commands[self.channel]:
                del _commands[self.channel]["play"]
            return "Auto adding deactivated"



def init(channel):
    _instances[channel] = Tracker(channel)

def notice_on_activate(channel):
    if _islocal and _instances[channel].infosavepath == "":
        return "If you want the current standings saved to text file, please set the path accordingly in the 'res/moduleconfig.json'."

@Command(required_permission=permissions.OWNER)
def maddplayer(channel, user, args):
    usage = "{}maddplayer <user>".format(config.cmdprefix)
    if len(args) != 1:
        return usage
    return _instances[channel].addPlayer(args[0])

@Command(required_permission=permissions.OWNER)
def maddpoints(channel, user, args):
    usage = "{}maddpoints <user> <points> (<user> <points> ...)".format(config.cmdprefix)
    if len(args) % 2 != 0:
        return usage
    try:
        pointlist = []
        for i in range(0, len(args), 2):
            pointlist.append((args[i], int(args[i+1])))
        return _instances[channel].addPoints(pointlist)
    except ValueError:
        return usage

@Command(required_permission=permissions.OWNER)
def mgetwinner(channel, user, args):
    usage = "{}mgetwinner <year> <month>".format(config.cmdprefix)
    if len(args) != 2:
        return usage
    try:
        year = int(args[0])
        month = int(args[1])
        return _instances[channel].pickWinner(year, month)
    except ValueError:
        return usage

@Command()
def mpoints(channel, user, args):
    usage = "{}mpoints (<username>)".format(config.cmdprefix)
    if len(args)>1:
        return usage
    return _instances[channel].getPoints(args[0] if len(args)==1 else user)

@Command()
def mrank(channel, user, args):
    usage = "{}mrank <rank>".format(config.cmdprefix)
    if len(args) != 1:
        return usage
    try:
        rank = int(args[0])
        if rank < 1:
            return "Rank must be greater than 0!"
        return _instances[channel].getRank(rank)
    except ValueError:
        return usage

@Command()
def mrigged(channel, user, args):
    usage = "{}mrigged".format(config.cmdprefix)
    if len(args) > 0:
        return usage
    return _instances[channel].getRigged()

@Command(required_permission=permissions.OWNER)
def mrenameplayer(channel, user, args):
    usage = "{}mrenameplayer <oldname> <newname>".format(config.cmdprefix)
    if len(args) != 2:
        return usage
    return _instances[channel].renamePlayer(args[0], args[1])

@Command(required_permission=permissions.OWNER)
def mautoadd(channel, user, args):
    usage = "{}mautoadd on/off".format(config.cmdprefix)
    try:
        b = utils.strtobool(args[0])
        return _instances[channel].autoadd(b)
    except:
        return usage
