from common import utils, config, permissions
from common.decorator import Command
import re

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']

VALID_COLORS = [ "blue", "blueviolet", "cadetblue", "chocolate", "coral",
    "dodgerblue", "firebrick", "goldenrod", "green", "hotpink", "orangered",
    "red", "seagreen", "springgreen", "yellowgreen"]
reobj = re.compile("[0-9A-Fa-f]{6}")

@Command(required_permission=permissions.OWNER)
def setchannelcolor(channel, user, args):
    usage = "Set color with {}setchannelcolor <hexcode>|<colorname>. Check for more info: https://help.twitch.tv/customer/portal/articles/659095-twitch-chat-and-moderation-commands".format(config.cmdprefix)
    if len(args) == 0:
        result = []
        if config.has("colors", channel):
            config.delete("colors", channel)
            result.append("Unset color for this channel.")
        result.append(usage)
        return result
    if len(args) > 1:
        return usage

    color = args[0]
    print("Color:", color)
    if color[0] == "#":
        if not reobj.match(color[1:]):
            return usage
    elif color.lower() not in VALID_COLORS:
        return usage

    config.set(color, "colors", channel)
    return "Color set to {}".format(color)
