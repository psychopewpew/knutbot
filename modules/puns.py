import random

from common import config, permissions, utils
from common.textlist import SimpleTextlist
from common.decorator import Command

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']

_textlist = SimpleTextlist("puns")

_blacklist = {}

def get_blacklist_config_path(channel):
    return ("puns", channel, "blacklist")

def init(channel):
    _blacklist[channel] = set(config.moduleconfig.get_default([], *get_blacklist_config_path(channel)))

@Command(required_permission=permissions.MOD)
def addpun(channel, user, args):
    usage = "Usage: {}addpun <pun>".format(config.cmdprefix)
    if len(args) == 0:
        return usage
    pun = ' '.join(args).strip()
    newid = _textlist.addEntry(pun)
    if newid != -1:
        return "{} Added it to the list with id {}.".format(utils.at_user(user), newid)
    else:
        return "{} That already exists.".format(utils.at_user(user))

@Command(required_permission=permissions.MOD)
def delpun(channel, user, args):
    usage = "Usage: {}delpun <id>".format(config.cmdprefix)
    if len(args) != 1:
        return usage
    try:
        entryid = int(args[0])
    except:
        return usage
    if _textlist.deleteEntry(entryid):
        return "Removed pun with id {}.".format(entryid)
    else:
        return "No pun with id {} exists.".format(entryid)

@Command(required_permission=permissions.MOD)
def editpun(channel, user, args):
    usage = "Usage: {}editpun <id> <new message>".format(config.cmdprefix)
    if len(args) < 2:
        return usage
    try:
        entryid = int(args[0])
    except:
        return usage
    if _textlist.editEntry(entryid, " ".join(args[1:])):
        return "Changed pun with id {}.".format(entryid)
    else:
        return "No pun with id {} exists.".format(entryid)

@Command(required_permission=permissions.ADMIN)
def loadpuns(channel, user, args):
    usage = "Usage: {}loadpuns <pathtofile>".format(config.cmdprefix)
    if len(args) != 1:
        return usage
    try:
        return "Loaded {} puns.".format(_textlist.loadEntries(args[0]))
    except FileNotFoundError:
        return "File {} does not exist.".format(args[0])

@Command()
def pun(channel, user, args):
    usage = "Usage: {}pun (<id>)".format(config.cmdprefix)
    if len(args) == 1:
        try:
            entryid = int(args[0])
        except:
            return usage
        if entryid in _blacklist[channel]:
            return "{} The pun was blacklisted.".format(utils.at_user(user))
        pun = _textlist.getEntry(entryid)
        if pun == None:
            return "No pun with id {} exists".format(entryid)
    elif len(args) == 0:
        pun = _textlist.getEntry(blacklist=_blacklist[channel])
        if pun == None:
            return "No puns available, add some with {}addpun .".format(config.cmdprefix)
    else:
        return usage
    return "{} {} (id: {})".format(utils.at_user(user), pun[1], pun[0])

@Command(required_permission=permissions.MOD)
def punblacklist(channel, user, args):  
    usage = "Usage: {}punblacklist add|remove <id>".format(config.cmdprefix)
    if len(args) != 2:
        return usage
    try:
        entryid = int(args[1])
    except:
        return usage
    if not _textlist.getEntry(entryid):
        return "No pun with id {} exists.".format(entryid)
    if args[0] == "add":
        _blacklist[channel].add(entryid)
        config.moduleconfig.set(list(_blacklist[channel]), *get_blacklist_config_path(channel))
        return "Added pun with id {} to blacklist".format(entryid)
    elif args[0] == "remove":
        if entryid in _blacklist[channel]:
            _blacklist[channel].remove(entryid)
            config.moduleconfig.set(list(_blacklist[channel]), *get_blacklist_config_path(channel))
            return "Removed pun with id {} from blacklist".format(entryid)
        else:
            return "Pun with id {} is not on the blacklist.".format(entryid)
    else:
        return usage 
    
