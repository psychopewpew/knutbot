import random

from common import config, permissions, utils
from common.textlist import SimpleTextlist
from common.decorator import Command

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']

_textlists = {
    "compliment": SimpleTextlist("compliments"),
    "insult": SimpleTextlist("insults")
}

def _addcritique(channel, user, args, ctype):
    if len(args) == 0:
        return "Usage: {0}add{1} <{1}>".format(config.cmdprefix, ctype)
    ctextlist = _textlists[ctype]
    critque = ' '.join(args).strip()
    newid = ctextlist.addEntry(critque)
    if newid != -1:
        return "{} Added {} to the list with id {}.".format(utils.at_user(user), ctype, newid)
    else:
        return "{} That {} already exists.".format(utils.at_user(user), ctype)

def _critique(channel, user, args, ctype, printtype=False):
    if len(args) != 1:
        return "Usage: {}{} <user>".format(config.cmdprefix, ctype)
    crit = _textlists[ctype].getEntry()
    if crit != None:
        return "{} {} ({}id: {})".format(utils.at_username(args[0]), crit[1], (ctype + " ") if printtype else "", crit[0])
    else:
        return "No {1}s available, add some with {0}add{1} .".format(config.cmdprefix, ctype)

def _delcritique(channel, user, args, ctype):
    if len(args) != 1:
        return "Usage: {}del{} <id>".format(config.cmdprefix, ctype)
    ctextlist = _textlists[ctype]
    try:
        entryid = int(args[0])
    except:
        return "Usage: {}del{} <id>".format(config.cmdprefix, ctype)
    if ctextlist.deleteEntry(entryid):
        return "Removed {} with id {}.".format(ctype, entryid)
    else:
        return "No {} with id {} exists.".format(ctype, entryid)

def _loadcritique(channel, user, args, ctype):
    if len(args) != 1:
        return "Usage: {}load{} <pathtofile>".format(config.cmdprefix, ctype)
    ctextlist = _textlists[ctype]
    try:
        return "Loaded {} {}s.".format(ctextlist.loadEntries(args[0]), ctype)
    except FileNotFoundError:
        return "File {} does not exist.".format(args[0])

@Command(required_permission=permissions.MOD)
def addcompliment(channel, user, args):
    return _addcritique(channel, user, args, "compliment")

@Command(required_permission=permissions.MOD)
def addinsult(channel, user, args):
    return _addcritique(channel, user, args, "insult")

@Command()
def compliment(channel, user, args):
    return _critique(channel, user, args, "compliment")

@Command(required_permission=permissions.MOD)
def delcompliment(channel, user, args):
    return _delcritique(channel, user, args, "compliment")

@Command(required_permission=permissions.MOD)
def delinsult(channel, user, args):
    return _delcritique(channel, user, args, "insult")

@Command()
def insult(channel, user, args):
    return _critique(channel, user, args, "insult")

@Command()
def judge(channel, user, args):
    return _critique(channel, user, args, "insult" if random.random() < 0.5 else "compliment", printtype=True)

@Command(required_permission=permissions.ADMIN)
def loadcompliment(channel, user, args):
    return _loadcritique(channel, user, args, "compliment")

@Command(required_permission=permissions.ADMIN)
def loadinsult(channel, user, args):
    return _loadcritique(channel, user, args, "insult")
