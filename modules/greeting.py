from common import permissions, config, timetracker, usertracker
from common.decorator import Command
import time

moduletype = config.moduletype["remote"] | config.moduletype["local"]
triggertype = config.triggertype['command'] | config.triggertype['message']

_instances = {}

class Greeter(object):

    def __init__(self, channel):
        self.channel = channel
        self._message = config.moduleconfig.get_default(None, "greeter",
            self.channel, "message")
        self._timediff = config.moduleconfig.get_default(None, "greeter",
            self.channel, "timediff")
        self._ignore = set(config.moduleconfig.get_default([], "greeter",
            self.channel, "ignorelist"))
        self.tracker = timetracker.TimeTracker("greeter_{}".format(channel[1:]))

    @property
    def message(self):
        return self._message

    @message.setter
    def message(self, message):
        self._message = message
        config.moduleconfig.set(message, "greeter", self.channel, "message")

    @property
    def timediff(self):
        return self._timediff

    @timediff.setter
    def timediff(self, seconds):
        self._timediff = seconds
        config.moduleconfig.set(seconds, "greeter", self.channel, "timediff")

    def ignore(self, username):
        self._ignore.add(username)
        config.moduleconfig.set(list(self._ignore), "greeter", 
            self.channel, "ignorelist")
        return True
        
    def unignore(self, username):
        if username in self._ignore:
            self._ignore.remove(username)
            config.moduleconfig.set(list(self._ignore), "greeter", 
                self.channel, "ignorelist")
            return True
        return False

    def is_setup(self):
        return self.message != None and self.timediff != None

    def greet_if_new(self, user):
        username = user.name
        if not self.is_setup():
            return None
        if username == self.channel[1:]:
            return None
        if username in self._ignore:
            return None
        timestamp = self.tracker.get_timestamp(username)
        curtime = time.time()
        self.tracker.update(username, curtime)
        if timestamp == None or curtime - timestamp > self.timediff:
            return self.message.format(user=str(user))

def init(channel):
    _instances[channel] = Greeter(channel)

def notice_on_activate(channel):
    if not _instances[channel].is_setup():
        return "You need to setup the time difference and message for greeting. \
            You can use {user} when wanting to replace it with the greeted user."

def trigger(channel, user, message, is_command):
    return _instances[channel].greet_if_new(user)

@Command(required_permission=permissions.OWNER)
def greetmsg(channel, user, args):
    if len(args) > 0:
        _instances[channel].message = " ".join(args)
        return "Set new message for greeting."
    else:
        return ("Greeting message: {}".format(_instances[channel].message),
            "Change with {}greetmsg <message>".format(config.cmdprefix))

@Command(required_permission=permissions.OWNER)
def greettime(channel, user, args):
    usage = "Change with {}greettime <minutes>".format(config.cmdprefix)
    if len(args) == 0:
        if _instances[channel].timediff:
            return ("Time since last talked to greet again: {} min".format(
                _instances[channel].timediff/60), usage)
        else:
            return usage
    elif len(args) == 1:
        try:
            minutes = int(args[0])
            _instances[channel].timediff = minutes * 60
            return "Set new time difference to {} min".format(minutes)
        except ValueError:
            return usage
    else:
        return usage
        
@Command(required_permission=permissions.OWNER)
def greetignore(channel, user, args):
    usage = "Add someone to ignore list with {}greetignore <user>".format(config.cmdprefix)
    if len(args) != 1:
        return usage
    username = args[0].lower()
    _instances[channel].ignore(username)
    return "Ignoring {} when greeting.".format(
        usertracker.gettracker(channel).get(username))
    
@Command(required_permission=permissions.OWNER)
def greetunignore(channel, user, args):
    usage = "Remove someone from the ignore list with {}greetunignore <user>".format(config.cmdprefix)
    if len(args) != 1:
        return usage
    username = args[0].lower()
    _instances[channel].unignore(username)
    return "Stopping ignoring {} when greeting.".format(
        usertracker.gettracker(channel).get(username))
