from common import config, permissions, usertracker
from common.decorator import Command
from common.twitchapi import helper
from common.twitchapi.exceptions import *

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']


@Command()
def uptime(channel, user, args):
    try:
        strimmer = usertracker.gettracker(channel).get(channel[1:])
        h, m, s = helper.uptime(strimmer)
        return f"{strimmer.displayname} is live for {h} hours, {m} minutes and {s} seconds"
    except UserNotLive as e:
        return str(e)


@Command()
def followage(channel, user, args):
    try:
        strimmer = usertracker.gettracker(channel).get(channel[1:])
        followdate = helper.followage(user, strimmer)
        return f"{user.displayname} is following {strimmer.displayname} since {followdate.strftime('%d %B %Y')}."
    except NotFollowing as e:
        return str(e)
