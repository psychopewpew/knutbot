from common import cooldown, permissions, config, utils
from common.decorator import Command
from .controller import command_active

moduletype = config.moduletype["remote"] | config.moduletype["local"]
triggertype = config.triggertype['command']

@Command(required_permission=permissions.MOD)
def setcooldown(channel, user, args):
    usage = "Usage: {}setcooldown <command> <type (global|user)> <time in seconds> (<ignore mods>)".format(config.cmdprefix) + "  ||  <ignore mods> is either 'true' or 'false', 'true' being the default"
    if len(args) != 3 and len(args) != 4:
        return usage
    command = args[0][1:] if "!" in args[0] else args[0]
    if not command_active(channel, command):
        return "Command {} is not avilable in your channel, not setting the cooldown.".format(command)
    t = args[1]
    if t != "global" and t != "user":
        return "Type must either be 'global' or 'user'."
    try:
        seconds = int(args[2])
    except:
        return "Cooldown must be seconds as integer."
    try:
        ignoremod = utils.strtobool(args[3]) if len(args) == 4 else True
    except:
        return "Ignore mod must be 'true' or 'false' (excluding the ' )"
    cd = {"cooldown": seconds, "type": t, "ignoremod": ignoremod}
    if cd["cooldown"] == 0:
        try:
            cooldown.cooldowns.delete(channel, command)
        except:
            pass
        return "Removed cooldown for command {}".format(command)
    cooldown.cooldowns.set(cd, channel, command)
    return "Set cooldown to {} seconds ({}) for command {} (mods ignore cooldown: {})".format(cd["cooldown"], cd["type"], command, ignoremod)

@Command(required_permission=permissions.MOD)
def listcooldowns(channel, user, args):
    if not cooldown.cooldowns.has(channel):
        return "No cooldowns set."
    cds = cooldown.cooldowns.get(channel)
    messages = []
    for cmd in cds:
        messages.append("{}: {}s ({}, ignored if mod: {})".format(cmd, cds[cmd]["cooldown"], cds[cmd]["type"], cds[cmd]["ignoremod"]))
    return utils.compress_message_list(messages, " | ")

