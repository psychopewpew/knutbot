from common.decorator import Command
from common import config, utils

import random

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']

MAX_ROLL = 20

DICE = [2,4,6,8,10,12,20,50,100]

_usage = ("{}roll <#dice>d<dice>".format(config.cmdprefix),
    "For example use '{}roll 3d20 1d8' to roll 3 dice 20 and 1 dice 8. Possible dice: {}".format(config.cmdprefix, ", ".join(map(str, DICE))))

@Command()
def roll(channel, user, args):
    if len(args) < 1:
        return _usage
    
    resultslist = ["{}".format(utils.at_user(user))]
    totalnumber = 0
    totalsum = 0
    for s in args:
        if "d" not in s:
            return _usage
        d_idx = s.index("d")
        try:
            number = int(s[:d_idx])
            dice = int(s[d_idx+1:])
        except:
            return _usage
        totalnumber += number
        if dice not in DICE:
            return _usage
        if totalnumber > MAX_ROLL:
            return "{} Maximum {} dice at once!".format(utils.at_user(user), MAX_ROLL)
        rolls = []
        for _ in range(number):
            rolls.append(random.randint(1, dice))
        resultslist.append("d{}: {} --".format(dice, ", ".join(map(str, rolls))))
        totalsum += sum(rolls)
    resultslist.append("sum: {}".format(totalsum))
    return " ".join(resultslist)
