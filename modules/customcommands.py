import re
import random
from common import config, utils, permissions, usertracker, genericdatabase
from common.decorator import Command, CustomCommand
from modules import controller
from common.twitchapi import helper as apihelper
from common.twitchapi import exceptions as apiexcept

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']

possible_args = {
"user": "user",
# ^ must start at start, $ must end at end
# do NOT match when string is e.g. "arg2a"
"arg": "^arg[0-9]+$", 
"argspecial": "arg[0-9]+:(user|channel|channelgame)",
"args": "args",
"random": "random:[0-9]+:[0-9]+" # random numbers
}

paranthesis_re = re.compile("\{.+?\}")

database = {}

ALLOWED_SLASH_COMMANDS = [
"/timeout", 
"/ban", 
"/unban", 
"/untimeout", 
"/me",
"/w {user}",
]


class ArgumentError(Exception):
    pass


class SlashError(Exception):
    pass
   

class ArgumentSpecialError(Exception):
    pass


class MoreArgumentsRequiredError(Exception):
    def __init__(self, number):
        self.number = number


def init(channel):
    database[channel] = genericdatabase.GenericDatabase("res/command.db", 
    channel[1:], [["command", "TEXT", "PRIMARY KEY"], ["response", "TEXT"], ["permission", "INTEGER"]])

    entries = database[channel].getall()
    for entry in entries:
        parsed_response, kwargdict = _parse_arguments(entry[1])
        _addcommand(channel, entry[0], parsed_response, kwargdict, \
        permissions.permdict[entry[2]])


def on_deactivate(channel):
    entries = database[channel].getall()
    for entry in entries:
        del controller._commands[channel][entry[0]]


def getall(channel):
    try:
        return database[channel].getall()
    except KeyError as e:
        return None


def _addcommand(channel, command, parsed_response, kwargdict, permission=permissions.EVERYONE):
    @CustomCommand(command, required_permission=permission)
    def customf(channel, user, args):
        try:
            kdict = {k: v[0](*v[1], channel, args) for k,v in kwargdict.items()}
            return parsed_response.format(user=user, args=" ".join(args), **kdict)
        except MoreArgumentsRequiredError as e:
            return "At least {} arguments are required to execute this command.\
                You can check the raw command with {}rawcommand {}".format(
                    e.number, config.cmdprefix, command)
        except apiexcept.UserDoesNotExist as e:
            return str(e)
    customf.custom = __name__
    customf.__name__ = command
    controller._commands[channel][command] = customf


def _getargx(argnumber, channel, args):
    try:
        return args[argnumber-1]
    except IndexError:
        raise MoreArgumentsRequiredError(argnumber)


def _getargxspecial(argnumber, special, channel, args):
    try:
        arg = args[argnumber-1]
        return {
            'user': lambda arg: usertracker.gettracker(channel).get(arg),
            'channel': lambda arg: "https://www.twitch.tv/"+utils.no_at_username(arg).lower(),
            'channelgame': lambda arg: _try_gamename(channel, arg)
            }[special](arg)
    except IndexError:
        raise MoreArgumentsRequiredError(argnumber)


def _parse_arguments(response):
    newresp = []
    restresp = response
    kwargdict = {}
    try:
        while True:
            match = paranthesis_re.search(restresp)
            if match is None:
                newresp.append(restresp)
                return "".join(newresp), kwargdict
            start, end = match.span()
            newresp.append(restresp[:start])
            argument = restresp[start:end].lower()[1:-1]
            if re.match(possible_args['random'], argument):
                splits = argument.split(':')
                argument = "kwarg{}".format(len(kwargdict))
                minmax = (int(splits[1]), int(splits[2]))
                if minmax[0] > minmax[1]:
                    raise ArgumentSpecialError("Minimum of random must be lower or equal to maximum!")
                kwargdict[argument] = (lambda min, max, *otherargs: random.randint(min, max), minmax)
            if re.match(possible_args['arg'], argument):
                argnumber = int(argument[3:])
                argument = "kwarg{}".format(len(kwargdict))
                kwargdict[argument] = (_getargx, (argnumber,))
            if re.match(possible_args['argspecial'], argument):
                splits = argument.split(':')
                argnumber = int(splits[0][3:])
                special = splits[1]
                argument = "kwarg{}".format(len(kwargdict))
                kwargdict[argument] = (_getargxspecial, (argnumber,special))
            newresp.append("{" + argument + "}")
            restresp = restresp[end:]
    except apiexcept.UserDoesNotExist as e:
        return str(e)
        


def _parse_response(response):
    if response.startswith("/") \
    and sum(map(lambda asc: response.startswith(asc), ALLOWED_SLASH_COMMANDS)) == 0:
        raise SlashError
    newresp = []
    restresp = response
    while True:
        match = paranthesis_re.search(restresp)
        if match == None:
            newresp.append(restresp)
            return "".join(newresp)
        start, end = match.span()
        newresp.append(restresp[:start])
        argument = restresp[start:end].lower()[1:-1]
        matchlist = {k: re.match(v, argument) for k, v in possible_args.items()}
        # Sanity check argument
        if not any(matchlist.values()):
            raise ArgumentError
        newresp.append("{" + argument + "}")
        restresp = restresp[end:]


def _try_gamename(channel, username):
    user = usertracker.gettracker(channel).get(username)
    return apihelper.get_gamename(user)


@Command(required_permission=permissions.VIP)
def addcommand(channel, user, args):
    usage = "Usage: {}addcommand <custom command> <response> || for formatting options check: {}modules/customcommands".format(config.cmdprefix, config.WIKI_URL)
    if len(args) < 2:
        return usage
    command = args[0].lower()
    if command.startswith(config.cmdprefix):
        command = command[len(config.cmdprefix):]
    # Check if command exists, and if it does if it is one from another module
    if command in controller._commands[channel] and \
    not hasattr(controller._commands[channel][command], "custom"):
        return "Command {} is a fixed command of a activated module.".format(command)

    newcmd = " ".join(args[1:])
    if newcmd.startswith("{args}"):
        return "A custom command cannot start with {args}."

    try:
        response = _parse_response(newcmd)
        parsed_response, kwargdict = _parse_arguments(response)
    except ArgumentError:
        return usage
    except SlashError:
        return "Only the following slash commands can be used: " + ", ".join(ALLOWED_SLASH_COMMANDS)
    except ArgumentSpecialError as e:
        return str(e)

    if not database[channel].add((True, False, False), (command, response, permissions.EVERYONE.level)):
        return "Custom command {} already exists".format(command)
    _addcommand(channel, command, parsed_response, kwargdict)

    return "Added command"


@Command(required_permission=permissions.MOD)
def delcommand(channel, user, args):
    if len(args) != 1:
        return "Usage: {}delcommand <custom command>".format(config.cmdprefix)
    command = args[0].lower()
    if command.startswith(config.cmdprefix):
        command = command[len(config.cmdprefix):]
        
    if database[channel].delete((True, False, False), (command, None, None)):
        del controller._commands[channel][command]
        return "Command {} deleted.".format(command)
    else:
        return "Command {} does not exist.".format(command)


@Command(required_permission=permissions.MOD)
def editcommand(channel, user, args):
    usage = "Usage: {}editcommand <custom command> <response> || for formatting options check: {}modules/customcommands".format(config.cmdprefix, config.WIKI_URL)
    if len(args) < 2:
        return usage
    command = args[0].lower()
    if command.startswith(config.cmdprefix):
        command = command[len(config.cmdprefix):]

    entry = database[channel].get((True, False, False), (command, None, None))
    if entry == None:
        return "Custom command {} doesnt exist".format(command)

    try:
        response = _parse_response(" ".join(args[1:]))
        parsed_response, kwargdict = _parse_arguments(response)
    except ArgumentError:
        return usage
    except SlashError:
        return "Only the following slash commands can be used: " + ", ".join(ALLOWED_SLASH_COMMANDS)
    except ArgumentSpecialError as e:
        return str(e)

    database[channel].edit((True, False, False), (command, None, None),
    (False, True, False), (None, response, None))
    _addcommand(channel, command, parsed_response, kwargdict, permissions.permdict[entry[2]])

    return "Changed the response for {}".format(command)


@Command()
def rawcommand(channel, user, args):
    usage = "Usage: {}rawcommand <custom command>".format(config.cmdprefix)
    if len(args) != 1:
        return usage
    command = args[0].lower()
    if command.startswith(config.cmdprefix):
        command = command[len(config.cmdprefix):]
    
    entry = database[channel].get((True, False, False), (command, None, None))
    if entry == None:
        return "Custom command {} doesnt exist".format(command)
    else:
        return entry[1]


@Command(required_permission=permissions.MOD)
def setcmdperm(channel, user, args):
    if len(args) < 2:
        return "Usage: {}setcommandperm <custom command> <permission level>".format(config.cmdprefix)
    command = args[0].lower()
    if command.startswith(config.cmdprefix):
        command = command[len(config.cmdprefix):]
    
    entry = database[channel].get((True, False, False), (command, None, None))
    if entry == None:
        return "Custom command {} doesnt exist".format(command)
    
    try:
        permobj = getattr(permissions, args[1].upper())
        if permobj > permissions.OWNER:
            raise Exception
    except:
        levels = sorted([l for l in list(permissions.permdict) if l <= 100])
        levelstrings = list(map(lambda l: str(permissions.permdict[l]), levels))
        return "Possible permission levels: " + ", ".join(levelstrings) 
    
    database[channel].edit((True, False, False), (command, None, None),
    (False, False, True), (None, None, permobj.level))
    
    parsed_response, kwargdict = _parse_arguments(entry[1])
    _addcommand(channel, command, parsed_response, kwargdict, permobj)
    
    return "Permission set to {}".format(permobj)
