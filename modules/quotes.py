import random
import time, datetime
from common import config, permissions, utils, usertracker
from common.textlist import GenericTextlist
from common.decorator import Command
from common.twitchapi import helper as twitchapi

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']

_instances = {}

class Quotes():
    
    def __init__(self, channel):
        self.channel = channel
        self.broadcaster = usertracker.gettracker(channel).get(channel[1:])
        self.textlist = GenericTextlist("res/quotes.db", "{}".format(channel[1:]), 
        [["game", "TEXT"], ["timestamp", "INTEGER"]])
        
    def add(self, quote):
        game = twitchapi.get_gamename(self.broadcaster)
        timestamp = time.time()
        newid = self.textlist.add(quote, (game, timestamp))
        return newid
        
    def get(self, idx=-1):
        return self.textlist.getEntry(idx)
    
    def delete(self, idx):
        return self.textlist.delete(idx)
        
    def edit(self, idx, newtext):
        return self.textlist.edit(idx, (False, True, False, False),
        (None, newtext, None, None))

def init(channel):
    _instances[channel] = Quotes(channel)

@Command(required_permission=permissions.EVERYONE)
def addquote(channel, user, args):
    usage = "Usage: {}addquote <quote>".format(config.cmdprefix)
    if len(args) == 0:
        return usage
    quote = ' '.join(args).strip()
    newid = _instances[channel].add(quote)
    if newid != -1:
        quote = _instances[channel].get(newid)
        date = datetime.datetime.utcfromtimestamp(quote[3]).strftime("%Y-%m-%d")
        return "{} Added: [{}] {} [{}] ({})".format(utils.at_user(user), 
        quote[0], quote[1], quote[2], date)
    else:
        return "{} That already exists.".format(utils.at_user(user))

@Command(required_permission=permissions.MOD)
def delquote(channel, user, args):
    usage = "Usage: {}delquote <id>".format(config.cmdprefix)
    if len(args) != 1:
        return usage
    try:
        entryid = int(args[0])
    except:
        return usage
    if _instances[channel].delete(entryid):
        return "Removed quote with id {}.".format(entryid)
    else:
        return "No quote with id {} exists.".format(entryid)


@Command(required_permission=permissions.MOD)
def editquote(channel, user, args):
    usage = "Usage: {}editquote <id> <new message>".format(config.cmdprefix)
    if len(args) < 2:
        return usage
    try:
        entryid = int(args[0])
    except:
        return usage
    if _instances[channel].edit(entryid, " ".join(args[1:])):
        quote = _instances[channel].get(entryid)
        date = datetime.datetime.utcfromtimestamp(quote[3]).strftime("%Y-%m-%d")
        return "{} Edited: [{}] {} [{}] ({})".format(utils.at_user(user), 
        quote[0], quote[1], quote[2], date)
    else:
        return "No quote with id {} exists.".format(entryid)

@Command()
def quote(channel, user, args):
    usage = "Usage: {}quote (<id>)".format(config.cmdprefix)
    if len(args) == 1:
        try:
            entryid = int(args[0])
        except:
            return usage
        quote = _instances[channel].get(entryid)
        if quote == None:
            return "No quote with id {} exists".format(entryid)
    elif len(args) == 0:
        quote = _instances[channel].get()
        if quote == None:
            return "No quote available, add some with {}addquote .".format(config.cmdprefix)
    else:
        return usage
    date = datetime.datetime.utcfromtimestamp(quote[3]).strftime("%Y-%m-%d")
    return "{} [{}] {} [{}] ({})".format(utils.at_user(user), quote[0],
    quote[1], quote[2], date)
