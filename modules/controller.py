import importlib
import traceback
import os

from common import permissions, config, utils, cooldown, commandtracker

_commands = {}
_loaded_modules = {}
_activated_modules = {}

_message_triggers = {}
_time_triggers = {}

_modules = []

def activate_module(module_name, channel, surpress_msg=False):
    return utils.wrap_and_change_color(channel, _activate_module(module_name, channel,surpress_msg))

def _activate_module(module_name, channel, surpress_msg):
    if module_name in _activated_modules[channel]:
        return "Module {} already activated".format(module_name)
    if module_name not in _modules:
        return "Module {} does not exist!".format(module_name)
    try:
        load_module(module_name)
    except ModeError as e:
        return str(e)
    module = _loaded_modules[module_name]
    if module.triggertype & config.triggertype['command'] > 0:
        for command in module.commandlist:
            _commands[channel][command] = getattr(module, command)
        _activated_modules[channel].add(module_name)
    if module.triggertype & config.triggertype['message'] > 0:
        _message_triggers[channel].add(module)
        _activated_modules[channel].add(module_name)
    if module.triggertype & config.triggertype['time'] > 0:
        _time_triggers[channel].add(module)
        _activated_modules[channel].add(module_name)
    if hasattr(module, "init"):
        module.init(channel)
    config.add(module_name,'modules',channel)

    if surpress_msg:
        return None

    notice = None
    if hasattr(module, "notice_on_activate"):
        notice = module.notice_on_activate(channel)

    if notice == None:
        return "Activated {}.".format(module_name)
    else:
        return "Activated {}.".format(module_name), notice

def collect_time_messages(channel):
    msges = []
    for module in _time_triggers[channel]:
        msges.extend(utils.wrap_in_tuple(module.collect_time_messages(channel)))
    msges = utils.wrap_and_change_color(channel, msges)
    return msges

def command_active(channel, command):
    return command in _commands[channel]

def deactivate_module(module_name, channel):
    return utils.wrap_and_change_color(channel, _deactivate_module(module_name, channel))

def _deactivate_module(module_name, channel):
    if module_name not in _activated_modules[channel]:
        return "Module {} is not activated".format(module_name)
    module = _loaded_modules[module_name]
    for command in module.commandlist:
        del _commands[channel][command]
    if hasattr(module, "on_deactivate"):
        module.on_deactivate(channel)
    config.remove(module_name,'modules',channel)
    _activated_modules[channel].remove(module_name)
    return "Deactivated {}".format(module_name)

def init():
    path = "modules/"
    files = [x for x in os.listdir(path) if os.path.isfile(path+x)]
    _modules.extend(sorted([ m[:-3] for m in files if os.path.basename(m).endswith('py')]))
    _modules.remove("__init__")
    _modules.remove("controller")
    if config.log_command_stats:
        global commandtracker
        commandtracker = commandtracker.CommandTracker()
    cooldown.start_cleanup_loop()

def init_channel(channel):
    _commands[channel] = {}
    _activated_modules[channel] = set()
    _message_triggers[channel] = set()
    _time_triggers[channel] = set()


def load_module(module_name):
    return utils.wrap_in_tuple(_load_module(module_name))

def _load_module(module_name):
    if module_name not in _loaded_modules:
        module = importlib.import_module("."+module_name, "modules")
        if module.moduletype & config.runtype == 0:
            run, mod = ('local', 'remote') if config.runtype == config.moduletype['local'] else ('remote','local')
            raise ModeError(module_name,  run, mod)
        _loaded_modules[module_name] = module


def runcommand(command, user, channel, args, whisper=False):
    if command_active(channel, command):
        return _commands[channel][command](channel, user, args, whisper)
    else:
        return ()


def runmessagetrigger(message, user, channel, is_command):
    answers = []
    for triggermodule in _message_triggers[channel]:
        answers.extend(utils.wrap_in_tuple(triggermodule.trigger(channel, 
            user, message, is_command)))
    answers = utils.wrap_and_change_color(channel, answers)
    return answers



class ModeError(Exception):

    def __init__(self, module, runningmode, modulemode):
        self.module = module
        self.runningmode = runningmode
        self.modulemode = modulemode

    def __str__(self):
        return "Cannot load module {}. It is declared {} but the bot is running in {} mode.".format(self.module,self.modulemode, self.runningmode)
