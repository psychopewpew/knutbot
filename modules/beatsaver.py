import json
import shutil
import os
import cfscrape
import zipfile

from common import utils, config, permissions
from common.decorator import Command

moduletype = config.moduletype["local"]
triggertype = config.triggertype['command']

_beatsaverstart = ["https://beatsaver.com/browse/detail/", "https://beatsaver.com/index.php/browse/detail/"]
_beatsaverdl = "https://beatsaver.com/storage/songs/{0}/{0}-{1}.zip"
_playlist = []

_custom_song_dir_config = ("beatsaver","customsongs_dir")
_allowed_config = ("beatsaver","allowed")
_maximum_requests_allowed_config = ("beatsaver","maxrequests","allowed")
_maximum_requests_nonallowed_config = ("beatsaver","maxrequests","non-allowed")
_currentplaylist = ("beatsaver","playlist")

_scraper = cfscrape.create_scraper()

class Songrequest():

    def __init__(self, user, song):
        self.user = user
        self.song = song

    def __eq__(self, other):
        if isinstance(other, str):
            return self.song == other
        return self == other

    def __str__(self):
        return "{} ({})".format(self.song, self.user)

def _config_is_set_up():
    if not config.moduleconfig.has(*_custom_song_dir_config) or config.moduleconfig.get(*_custom_song_dir_config) == "":
        config.moduleconfig.load()
        if not config.moduleconfig.has(*_custom_song_dir_config) or config.moduleconfig.get(*_custom_song_dir_config) == "":
            config.moduleconfig.set("",*_custom_song_dir_config)
            return False
    return True

def _download(dlurl):

    customdir = config.moduleconfig.get(*_custom_song_dir_config)
    if customdir[-1] != os.sep:
        customdir += os.sep
        config.moduleconfig.set(customdir,*_custom_song_dir_config)

    name = dlurl.split('/')[-1]
    extractdir = name[:name.index('.')]
    if os.path.exists(customdir + extractdir):
        return

    cfurl = _scraper.get(dlurl).content

    with open(name, 'wb') as f:
        f.write(cfurl)

    zip_ref = zipfile.ZipFile(name, 'r')
    zip_ref.extractall(extractdir)
    zip_ref.close()

    os.remove(name)
    shutil.move(extractdir, config.moduleconfig.get(*_custom_song_dir_config))

def _list_limit_reached(channel, user):
    userlevel = config.get_default(0, 'permissions',channel,user)
    allowed = userlevel >= permissions.MOD or user in config.moduleconfig.get(*_allowed_config)
    limit = config.moduleconfig.get(*_maximum_requests_allowed_config) if allowed else config.moduleconfig.get(*_maximum_requests_nonallowed_config)
    for req in _playlist:
        if req.user == user:
            limit -= 1
            if limit == 0:
                return True
    return False

@Command(required_permission=permissions.MOD)
def bsallow(channel, user, args):
    if len(args) != 1:
        return "Usage: {}bsallow <user>"
    config.moduleconfig.add(args[0].lower(), *_allowed_config)
    return "{} Added {} to the list of who is allowed to request more songs.".format(utils.at_user(user), args[0])

@Command(required_permission=permissions.MOD)
def bscontinue(channel, user, args):
    usage = "Usage: {}bscontinue <#songs to remove from list>".format(config.cmdprefix)
    global _playlist
    if len(args) != 1:
        return usage
    try:
        n = min(len(_playlist), int(args[0]))
    except:
        return usage
    _playlist = _playlist[n:]
    config.moduleconfig.set(list(map(str, _playlist)), *_currentplaylist)
    return "Moved {} songs forward.".format(n), bslist(channel, user, args)

@Command()
def bslist(channel, user, args):
    return 'Next songs: ' + ' | '.join([str(r) for r in _playlist[:min(5,len(_playlist))]])

@Command()
def bsplay(channel, user, args):
    if not _config_is_set_up():
        return("Please configure the custom songs directory in the 'res/moduleconfig.json' file and try again.")

    if _list_limit_reached(channel, user):
        return "@{} You already have the maximum number of songs in the queue.".format(user)

    usage = "Browse for songs on https://beatsaver.com/ || Usage: {0}bsplay <Detailpage URL> || For example: {0}bsplay https://beatsaver.com/browse/detail/1184-815".format(config.cmdprefix)
    if len(args) != 1:
        return usage
    link = args[0]
    linktype = -1
    for x in range(len(_beatsaverstart)):
        if link.startswith(_beatsaverstart[x]):
            linktype = x
    if linktype == -1:
        return usage
    start = _beatsaverstart[linktype]
    numberstring = link[len(start):]
    sepindex = numberstring.index('-')
    firstnumber = numberstring[:sepindex]
    secondnumber = numberstring[sepindex+1:]

    dlurl = _beatsaverdl.format(firstnumber,secondnumber)
    _download(dlurl)

    name = dlurl.split('/')[-1]
    extractdir = name[:name.index('.')]

    customdir = config.moduleconfig.get(*_custom_song_dir_config)
    if os.path.isfile(customdir+extractdir + os.sep + "info.json"):
        infopath = customdir+extractdir + os.sep + "info.json"
    else:
        infopath = customdir+extractdir + os.sep + os.listdir(customdir+extractdir)[0] + os.sep + "info.json"

    with open(infopath, "r") as f:
        info = json.loads(' '.join(f.readlines()))
        songname = info["songName"]

    if songname not in _playlist:
        _playlist.append(Songrequest(user, songname))
        config.moduleconfig.set(list(map(str, _playlist)), *_currentplaylist)
        return "Added '{}' to the playlist".format(songname)
    return "'{}' already in the playlist".format(songname)

def notice_on_activate(channel):
    if not config.moduleconfig.has(*_custom_song_dir_config) or config.moduleconfig.get(*_custom_song_dir_config) == "":
        config.moduleconfig.set("",*_custom_song_dir_config)
        return("Configure the custom songs directory in the 'res/moduleconfig.json' file to use this module.")

if not config.moduleconfig.has(*_maximum_requests_nonallowed_config):
    config.moduleconfig.set(1, *_maximum_requests_nonallowed_config)
if not config.moduleconfig.has(*_maximum_requests_allowed_config):
    config.moduleconfig.set(5, *_maximum_requests_allowed_config)
if not config.moduleconfig.has(*_allowed_config):
    config.moduleconfig.set([], *_allowed_config)
if config.moduleconfig.has(*_currentplaylist):
    l = config.moduleconfig.get(*_currentplaylist)
    for entry in l:
        idx = entry.rfind("(")
        song = entry[:idx-1]
        user = entry[idx+1:-1]
        sr = Songrequest(user, song)
        _playlist.append(sr)
