import requests
import re
from common import config, utils, permissions, usertracker, genericdatabase, scheduledqueue
from common.decorator import Command, CustomCommand
from modules import controller

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']

DATABASE_FILENAME = "res/customrequests.db"

database = {}


        
class RetrieveSetError(Exception):
    pass

class Requests(object):
    
    def __init__(self, channel, customreq, seturl=None, recent_limit=0, 
        queuetype="PrimitiveQueue"):
        self.channel = channel
        self.broadcaster = channel[1:]
        self.requestname = customreq
        self.set_seturl(seturl, False)
        self.commands = None
        configpath = ("customrequests", channel, customreq)
        self.queue = getattr(scheduledqueue, queuetype)(recent_limit, 
            configpath)
        self.core_db = database[channel]["core"]
    
    def format_request(self, tag):
        if self.set:
            return "[{}] {}".format(tag, self.set_entry(tag))
        else:
            return tag
            
    def set_entry(self, tag):
        if not self.set:
            return tag
        if tag in self.set:
            return self.set[tag]
        else:
            return "INVALID"
    
    def current(self):
        current = self.queue.current
        if current:
            tag = current.entry
            requester = current.user
            req = self.format_request(tag)
            return "{} (by {})".format(req, requester)
        else:
            return None
        
    def add(self, request, user):
        if request.startswith("["):
            request = request[1:-1]
        
        if self.set and request not in self.set:
            self.retrieve_set()
            if self.set and request not in self.set:
                raise KeyError(request)
        formatted_request = self.set_entry(request)
        position = self.queue.add(user, request)
        return formatted_request, position
            
    
    def clear(self):
        self.queue.clear()
        
    def delete(self):
        self.db.drop()
    
    def forward_request(self):
        return self.queue.forward()
            
    def retrieve_set(self):
        resp = requests.get(self.seturl)
        if resp.status_code == requests.codes.ok:
            self.set = {}
            lines = resp.text.splitlines()
            if lines and lines[0].startswith("<!DOCTYPE"):
                self.set = None
                self.seturl = None
                raise RetrieveSetError
            for line in lines:
                if not line.startswith("["):
                    continue
                try:
                    closeidx = line.index("]")
                except ValueError:
                    continue
                tag = line[1:closeidx]
                description = line[closeidx+1:].strip()
                self.set[tag] = description
        else:
            raise RetrieveSetError
    
    def set_queue_type(self, queuetype):
        recent_limit = self.queue.recent_limit
        configpath = ("customrequests", self.channel, self.requestname)
        self.queue = getattr(scheduledqueue, queuetype)(recent_limit, 
            configpath)
    
    def set_recent_limit(self, limit):
        self.queue.recent_limit = limit
    
    def set_seturl(self, seturl, commit=True):
        try:
            self.seturl = seturl
            self.set = None
            if self.seturl:
                try:
                    self.retrieve_set()
                except RetrieveSetError:
                    self.set = None
                    self.seturl = None
                    raise
        finally:
            if commit:
                self.core_db.edit((True, False, False), (self.requestname, 
                None, None), (False, True, False), (None, self.seturl, None))
                
    def upcoming(self):
        allreq = self.queue.upcoming(3)
        upcominglist = []
        for request in allreq:
            tag = request.entry
            requester = request.user
            req = self.format_request(tag)
            upcominglist.append("{} (by {})".format(req, requester))
        return " - ".join(upcominglist)

def init(channel):
    database[channel] = {}
    database[channel]["core"] = genericdatabase.GenericDatabase(
        DATABASE_FILENAME, 
        channel[1:], 
        [  
            ["command", "TEXT", "PRIMARY KEY"],
            ["seturl","TEXT"], 
            ["recent_limit", "INTEGER"], 
            ["queue_type", "TEXT"]
        ])
    entries = database[channel]["core"].getall()
    for entry in entries:
        _add_customrequest(channel, *entry)
        
def on_deactivate(channel):
    entries = database[channel]["core"].getall()
    for entry in entries:
        reqobj = database[channel][entry[0]]
        for cmd in reqobj.commands:
            del controller._commands[channel][cmd]

def _add_customrequest(channel, customreq, seturl=None, recent_limit=0,
    queue_type="PrimitiveQueue"):
    database[channel][customreq] = Requests(
        channel, 
        customreq, 
        seturl, 
        recent_limit if recent_limit else 0, 
        queue_type
        )
    commands = []
    commands.append(_create_req_command(channel, customreq))
    commands.append(_create_forward_command(channel, customreq))
    commands.append(_create_current_command(channel, customreq))
    commands.append(_create_seturl_command(channel, customreq))
    commands.append(_create_recent_command(channel, customreq))
    commands.append(_create_clear_command(channel, customreq))
    commands.append(_create_upcoming_command(channel, customreq))
    commands.append(_create_setqueuetype_command(channel, customreq))
    database[channel][customreq].commands = commands
    return commands

def _create_req_command(channel, customreq):
    command = "req"+customreq
    @CustomCommand(command)
    def _custom(channel, user, args):
        reqobj = database[channel][customreq]
        if len(args) < 1:
            if reqobj.seturl:
                return "Usage: {}{} <option> || Check {} for options".format(
                config.cmdprefix, command, reqobj.seturl)
            else:
                return "Usage: {}{} <request>".format(config.cmdprefix,
                command)
        request = " ".join(args)
        try:
            formatted_request, listentry = reqobj.add(request, user)
            return "Added '{}' to position {}".format(
            formatted_request, listentry)
        except scheduledqueue.UpcomingRequestError as e:
            return "'{}' is already in the queue at position {}".format(
            e.entry, e.current_position)
        except KeyError:
            return "'{}' does not exist as option. Check {} for options".format(
            request, reqobj.seturl)
        except scheduledqueue.RecentRequestError as e:
            return "'{}' was requested recently. Try again later.".format(
            e.entry)
        except scheduledqueue.CurrentRequestError as e:
            return "'{}' is the current request.".format(e.entry)
        except RetrieveSetError:
            return "There was an error trying to update the set because \
            {} was not found as option locally. Please try again.".format(request)
            
    _setup_command(channel, _custom, command)
    return command
    
def _create_forward_command(channel, customreq):
    command = "next"+customreq
    @CustomCommand(command, required_permission=permissions.MOD)
    def _custom(channel, user, args):
        reqobj = database[channel][customreq]
        if reqobj.forward_request():
            return "Next request: " + reqobj.current()
        else:
            return "Currently no requests are in queue."
    
    _setup_command(channel, _custom, command)
    return command
    
def _create_clear_command(channel, customreq):
    command = "clear"+customreq
    @CustomCommand(command, required_permission=permissions.OWNER)
    def _custom(channel, user, args):
        database[channel][customreq].clear()
        return "Cleared all requests."
            
    _setup_command(channel, _custom, command)
    return command
    
def _create_current_command(channel, customreq):
    command = "curr"+customreq
    @CustomCommand(command)
    def _custom(channel, user, args):
        current = database[channel][customreq].current()
        if current:
            return "Current request: {}".format(current)
        else:
            return "There is no current request."
            
    _setup_command(channel, _custom, command)
    return command
    
def _create_recent_command(channel, customreq):
    command = "set"+customreq+"recent"
    @CustomCommand(command, required_permission=permissions.OWNER)
    def _custom(channel, user, args):
        usage = "Usage: {}{} <recent limit>".format(config.cmdprefix, command)
        try:
            limit = int(args[0])
        except:
            return usage
        database[channel][customreq].set_recent_limit(limit)
        return "Successfully set the recent limit to {}".format(limit)
            
    _setup_command(channel, _custom, command)
    return command
    
qtypedict = {
    "primitive":    "PrimitiveQueue",
    "user":         "UserBasedQueue",
    "subuser":      "SubUserBasedQueue",
}
def _create_setqueuetype_command(channel, customreq):
    command = "set"+customreq+"queuetype"
    @CustomCommand(command, required_permission=permissions.OWNER)
    def _custom(channel, user, args):
        usage = "Usage: {}{} [primitive|user|subuser]".format(
            config.cmdprefix, command)
        if len(args) != 1:
            return usage
        qtype = args[0]
        if qtype not in qtypedict:
            return usage
        database[channel][customreq].set_queue_type(qtypedict[qtype])
        return "Changed queue type to {}.".format(qtypedict[qtype])
    _setup_command(channel, _custom, command)
    return command
        
    
def _create_seturl_command(channel, customreq):
    command = "set"+customreq+"options"
    @CustomCommand(command, required_permission=permissions.OWNER)
    def _custom(channel, user, args):
        if len(args) > 2:
            return "Usage: {}{} (<url>) || Provide url to set options, \
                omit it to remove the predefined options.".format(
                config.cmdprefix, command)
        url = args[0] if len(args) == 1 else None
        try:
            database[channel][customreq].set_seturl(url)
            return "Successfully set options."
        except RetrieveSetError:
            return "There was an error retrieving the options. Make sure you provide a link to a plaintext file."
            
    _setup_command(channel, _custom, command)
    return command
    
def _create_upcoming_command(channel, customreq):
    command = "upcoming"+customreq
    @CustomCommand(command)
    def _custom(channel, user, args):
        upcoming = database[channel][customreq].upcoming()
        if upcoming:
            return "Upcoming requests: {}".format(upcoming)
        else:
            return "There are no requests in the queue."
            
    _setup_command(channel, _custom, command)
    return command
    
def _setup_command(channel, function, command):
    function.custom = __name__
    function.__name__ = command
    controller._commands[channel][command] = function

@Command(required_permission=permissions.OWNER)
def addcustomrequest(channel, user, args):
    if len(args) != 1:
        return "Usage: {}addcustomrequest <requesttype>".format(config.cmdprefix)
    reqtype = args[0]
    if database[channel]["core"].add((True, False, False), (reqtype, None, None)):
        commands = map(
            lambda c: "{}{}".format(config.cmdprefix, c),
            _add_customrequest(channel, reqtype)
            )
        return "Added the request type. New commands: " + " ".join(commands)
    else:
        return "That request type already exists."
    
@Command(required_permission=permissions.OWNER)
def delcustomrequest(channel, user, args):
    if len(args) != 1:
        return "Usage: {}delcustomrequest <requesttype>".format(config.cmdprefix)
    reqtype = args[0]
    if database[channel]["core"].delete((True, False, False), (reqtype, None, None)):
        reqobj = database[channel][reqtype]
        reqobj.delete()
        for cmd in reqobj.commands:
            del controller._commands[channel][cmd]
        del database[channel][reqtype]
        return "Deleted the request type."
    else:
        return "The request type doesn't exist."
