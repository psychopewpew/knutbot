from common import config
import random

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['message']

PROBABILITY = 0.01

def trigger(channel, user, message, is_command):
    if not is_command and random.random() < PROBABILITY:
        return "^^"
