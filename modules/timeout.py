from common import config, permissions
from common.decorator import Command

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']

_usage = "Usage: {}{} <user> to timeout user for {} minutes"


def _timeout(channel, user_to_timeout, cmd, time_in_s):
    msg = config.moduleconfig.get_default("", "timeout", channel, cmd+"-msg")
    result = ["/timeout {} {}".format(user_to_timeout, time_in_s)]
    if len(msg) > 0:
        result.append(msg)
    return tuple(result)

@Command()
def abort(channel, user, args):
    return _timeout(channel, user, "abort", 1)

@Command(required_permission=permissions.MOD)
def slap(channel, user, args):
    if len(args) != 1:
        return _usage.format(config.cmdprefix, "slap", 10)
    return _timeout(channel, args[0], "slap", 600)

@Command(required_permission=permissions.MOD)
def tap(channel, user, args):
    if len(args) != 1:
        return _usage.format(config.cmdprefix, "tap", 1)
    return _timeout(channel, args[0], "tap", 60)

@Command(required_permission=permissions.OWNER)
def timeoutmsg(channel, user, args):
    usage = "Usage: {}timeoutmsg slap|tap <message> (leave message empty to clear the message)".format(config.cmdprefix)
    if len(args) == 0:
        return usage
    cmd = args[0]
    if cmd != "slap" and cmd != "tap":
        return usage
    newmsg = " ".join(args[1:])
    config.moduleconfig.set(newmsg, "timeout", channel, cmd+"-msg")
    if len(newmsg) == 0:
        return "Removed the message for {}.".format(cmd)
    else:
        return "Set the message for {}.".format(cmd)
