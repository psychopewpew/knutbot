import sys
from common import utils, config, permissions, pointtracker
from common.decorator import Command

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command'] | config.triggertype["message"]

_instances = {}

_config_save_infos_path = ("marbles", "path_to_save_infos")

class GuessTracker:
    commandmode = 0b1
    messagemode = 0b10
    bothmode = commandmode | messagemode

    def __init__(self, channel):
        self.channel = channel
        self.pointtracker = pointtracker.PointTracker("res/guessing_{}.db".format(channel[1:]), "")

        if not config.moduleconfig.has("guesses", self.channel, "mode"):
            config.moduleconfig.set(self.bothmode, "guesses", self.channel, "mode")
        self.mode = config.moduleconfig.get("guesses", self.channel, "mode")

        #if not config.moduleconfig.has("guesses", self.channel, "messages_between_topic_notice"):
        #    config.moduleconfig.set(25, "guesses", self.channel, "messages_between_topic_notice")
        self.messages_to_notice = 25#config.moduleconfig.get("guesses", self.channel, "messages_between_topic_notice")

        self.setSeason(config.moduleconfig.get_default(1, "guesses", self.channel, "season"))

        self.running = config.moduleconfig.get_default(False, "guesses", self.channel, "running")

    def addGuess(self, guess):
        if self.running:
            return "Cannot add phrases while a game is running."
        guesses = config.moduleconfig.get_default([], "guesses", self.channel, "guesses")
        if guess not in guesses:
            guesses.append(guess)
            config.moduleconfig.set(guesses, "guesses", self.channel, "guesses")
            return "Added {} to guesses ({} total)".format(guess, self.getTotalGuesses())
        else:
            return "{} already in guesses".format(guess)

    def addPoint(self, username):
        player = self.pointtracker.getPlayer(username)
        if not player:
            self.pointtracker.addPlayer(username)
            player = self.pointtracker.getPlayer(username)
        return "{0} wins ({2} wins total)".format(*self.pointtracker.addPoints(player, 1))

    def checkWinner(self, username, word):
        guesses = config.moduleconfig.get_default([], "guesses", self.channel, "guesses")
        if word in guesses:
            config.moduleconfig.remove(word, "guesses", self.channel, "guesses")
            config.moduleconfig.add((username, word), "guesses", self.channel, "winners")
            if self.getTotalGuesses() > 0:
                return self.addPoint(username) + " -- {} phrases to guess left!".format(self.getTotalGuesses())
            else:
                result = [self.addPoint(username), "Guessing ended, all phrases were guessed!"]
                result.extend(self.end())
                return tuple(result)
        else:
            self.messages_to_notice -= 1
            if self.messages_to_notice <= 0:
                self.messages_to_notice = config.moduleconfig.get("guesses", self.channel, "messages_between_topic_notice")
                return config.moduleconfig.get("guesses", self.channel, "topic")

    def clear(self):
        config.moduleconfig.delete("guesses", self.channel, "guesses")
        config.moduleconfig.delete("guesses", self.channel, "winners")
        config.moduleconfig.delete("guesses", self.channel, "topic")

    def end(self):
        if not self.running:
            return "No guessing round running right now."
        self.running = False
        config.moduleconfig.set(self.running, "guesses", self.channel, "running")
        response = []
        guesses = config.moduleconfig.get_default([], "guesses", self.channel, "guesses")
        if len(guesses) > 0:
            response.append("/me NOT GUESSED PHRASES")
            i = 0
            resp = guesses[i]
            i += 1
            while i < len(guesses):
                if len(resp) + 3 + len(guesses[i]) < config.irc_text_limit:
                    resp += " | " + guesses[i]
                else:
                    response.append(resp)
                    resp = guesses[i]
                i += 1
            response.append(resp)
        winners = config.moduleconfig.get_default([], "guesses", self.channel, "winners")
        if len(winners) > 0:
            response.append("/me GUESSED PHRASES")
            i = 0
            resp = "{1} (by {0})".format(*winners[i])
            i += 1
            while i < len(winners):
                if len(resp) + 3 + len(winners[i][0]) + 9 + len(winners[i][1]) < config.irc_text_limit:
                    resp += " | " + "{1} (by {0})".format(*winners[i])
                else:
                    response.append(resp)
                    resp = " | " + "{1} (by {0})".format(*winners[i])
                i += 1
            response.append(resp)
        self.clear()
        return tuple(response)

    def getPoints(self, username):
        player = self.pointtracker.getPlayer(username)
        if player == None:
            return "Player {} does not exist!".format(username)
        if not self.pointtracker.currentTableExists():
            return "{} nobody has won in the current season.".format(username)
        standings = self.pointtracker.getStandings()
        sortstandings = sorted(standings, key=lambda x: x[1], reverse=True)
        rank = 0
        curpoints = sys.maxsize
        step = 1
        for place in sortstandings:
            if place[1] < curpoints:
                rank += step
                curpoints = place[1]
                step = 1
            else:
                step += 1
            if place[0].lower() == username.lower():
                return "{}: {} points (Rank: {})".format(place[0], place[1], rank)
        return "Player {} never won in the current season.".format(username)

    def getRank(self, rank):
        if not self.pointtracker.currentTableExists():
            return "{} nobody has won in the current season.".format(username)
        standings = self.pointtracker.getStandings()
        if len(standings) < rank:
            return "Rank {} does not exist (lowest rank is {})".format(rank, len(standings))
        sortstandings = sorted(standings, key=lambda x: x[1], reverse=True)
        currank = 0
        curpoints = sys.maxsize
        step = 1
        for place in sortstandings:
            if place[1] < curpoints:
                currank += step
                curpoints = place[1]
                step = 1
            else:
                step += 1
            if rank == currank:
                return "{}: {} points (Rank: {})".format(place[0], place[1], currank)
            elif currank+step > rank:
                return "{}: {} points (Rank: {}) -- There are {} players with rank {}".format(place[0], place[1], currank, step, currank)
        return "Rank not found, should never happen BibleThump"

    def getRigged(self):
        if not self.pointtracker.currentTableExists():
            return "Nobody has won in the current season."
        standings = self.pointtracker.getStandings()
        sortstandings = sorted(standings, key=lambda x: x[1], reverse=True)[:5]
        texts = []
        for place in sortstandings:
            texts.append("{}: {}".format(place[0], place[1]))
        return "Top 5 players: " + ", ".join(texts)

    def getTopic(self):
        if config.moduleconfig.has("guesses", self.channel, "topic"):
            return config.moduleconfig.get("guesses", self.channel, "topic")
        else:
            return "No topic set yet. Set it with '{}gtopic <topic>'.".format(config.cmdprefix)

    def getTotalGuesses(self):
        return len(config.moduleconfig.get_default([], "guesses", self.channel, "guesses"))

    def setMode(self, mode):
        self.mode = mode

    def setSeason(self, season):
        config.moduleconfig.set(season, "guesses", self.channel, "season")
        self.pointtracker.current_table = "`season_{}`".format(season)
        return "Switched to season {}".format(config.moduleconfig.get("guesses", self.channel, "season"))

    def setTopic(self, topic):
        config.moduleconfig.set(topic, "guesses", self.channel, "topic")
        return "Topic was set."

    def start(self):
        if not config.moduleconfig.has("guesses", self.channel, "topic"):
            return "No topic set yet. Set it first using '{}gtopic <topic>'".format(config.cmdprefix)
        if len(config.moduleconfig.get_default([], "guesses", self.channel, "guesses")) == 0:
            return "No guessing phrases were added!"
        self.running = True
        config.moduleconfig.set(self.running, "guesses", self.channel, "running")
        return "Guessing started! There are {} phrases to guess".format(self.getTotalGuesses()), config.moduleconfig.get("guesses", self.channel, "topic")

def init(channel):
    _instances[channel] = GuessTracker(channel)

def trigger(channel, user, message, is_command):
    if _instances[channel].mode & GuessTracker.messagemode > 0 and _instances[channel].running:
        return _instances[channel].checkWinner(user.name, " ".join(message).lower())

@Command(required_permission=permissions.OWNER)
def gadd(channel, user, args):
    usage = "{}gadd <guessing phrase>".format(config.cmdprefix)
    if len(args) < 1:
        return usage
    return _instances[channel].addGuess(" ".join(args).lower())

@Command(required_permission=permissions.OWNER)
def gmode(channel, user, args):
    usage = "{}gmode (<mode>)".format(config.cmdprefix)
    if len(args) > 2:
        return usage
    if len(args) == 1:
        mode = args[0].lower()
        if mode not in ["command", "message", "both"]:
            return usage, "Valid modes: command, message, both"
        if mode == "command":
            _instances[channel].setMode(GuessTracker.commandmode)
        elif mode == "message":
            _instances[channel].setMode(GuessTracker.messagemode)
        else:
            _instances[channel].setMode(GuessTracker.bothmode)
        return "Set mode to {}".format(mode)
    else:
        if _instances[channel].mode == GuessTracker.commandmode:
            curmode = "command"
        elif _instances[channel].mode == GuessTracker.messagemode:
            curmode = "message"
        else:
            curmode = "both (command + message)"
        return "Current mode is {}. Change with {}gmode <mode>".format(curmode, config.cmdprefix)

@Command(required_permission=permissions.OWNER)
def gend(channel, user, args):
    usage = "{}gend".format(config.cmdprefix)
    if len(args) > 0:
        return usage
    return _instances[channel].end()

@Command(required_permission=permissions.OWNER)
def gswitchseason(channel, user, args):
    usage = "{}gswitchseason <seasonnumber>".format(config.cmdprefix)
    if len(args) != 1:
        return usage
    try:
        return _instances[channel].setSeason(int(args[0]))
    except:
        return usage

@Command()
def gseason(channel, user, args):
    usage = "{}gseason".format(config.cmdprefix)
    if len(args) > 0:
        return usage
    return "Current season: {}".format(_instances[channel].season)

@Command()
def gpoints(channel, user, args):
    usage = "{}gpoints (<username>)".format(config.cmdprefix)
    if len(args)>1:
        return usage
    return _instances[channel].getPoints(args[0] if len(args)==1 else user)

@Command()
def grank(channel, user, args):
    usage = "{}mrank <rank>".format(config.cmdprefix)
    if len(args) != 1:
        return usage
    try:
        rank = int(args[0])
        if rank < 1:
            return "Rank must be greater than 0!"
        return _instances[channel].getRank(rank)
    except ValueError:
        return usage

@Command()
def grigged(channel, user, args):
    usage = "{}mrigged".format(config.cmdprefix)
    if len(args) > 0:
        return usage
    return _instances[channel].getRigged()

@Command(required_permission=permissions.OWNER)
def gstart(channel, user, args):
    usage = "{}gstart".format(config.cmdprefix)
    if len(args) > 0:
        return usage
    return _instances[channel].start()

@Command(required_permission=permissions.OWNER)
def gtopic(channel, user, args):
    if len(args) == 0:
        return _instances[channel].getTopic()
    else:
        return _instances[channel].setTopic(" ".join(args))

@Command()
def guess(channel, user, args):
    if not _instances[channel].running:

        return "No guessing round running right now."
    if _instances[channel].mode & GuessTracker.commandmode > 0:
        usage = "{}guess <guess>".format(config.cmdprefix)
        if len(args) < 1:
            return usage
        return _instances[channel].checkWinner(user, " ".join(args).lower())
    else:
        return "Guesses just go by messages. Type your guess as a normal message."

