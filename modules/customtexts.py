import random

from common import config, permissions, utils, genericdatabase
from common.textlist import SimpleTextlist
from common.decorator import Command, CustomCommand
from modules import controller

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']

DATABASE_FILENAME = "res/customsimpletext.db"

_textlists = {}
database = {}

def init(channel):
    database[channel] = genericdatabase.GenericDatabase(DATABASE_FILENAME, 
    channel[1:], [["command", "TEXT", "PRIMARY KEY"]])
    entries = database[channel].getall()
    for entry in entries:
        _add_customtext(channel, entry[0])
        
def on_deactivate(channel):
    entries = database[channel].getall()
    for entry in entries:
        _del_customtext(channel, entry[0])
    
def _add_customtext(channel, customtext):
    if customtext not in _textlists:
        _textlists[customtext] = SimpleTextlist("text_"+customtext, DATABASE_FILENAME)
    _create_add_command(channel, customtext)
    _create_get_command(channel, customtext)
    _create_edit_command(channel, customtext)
    _create_del_command(channel, customtext)
    
def _create_add_command(channel, customtext):
    addcommand = "add"+customtext
    @CustomCommand(addcommand, required_permission=permissions.MOD)
    def _customtextadd(channel, user, args):
        if len(args) == 0:
            return "Usage: {0}add{1} <{1}>".format(config.cmdprefix, customtext)
        ctextlist = _textlists[customtext]
        critque = ' '.join(args).strip()
        newid = ctextlist.addEntry(critque)
        if newid != -1:
            return "{} Added {} to the list with id {}.".format(utils.at_user(user), customtext, newid)
        else:
            return "{} That {} already exists.".format(utils.at_user(user), customtext)
            
    _customtextadd.custom = __name__
    _customtextadd.__name__ = addcommand
    controller._commands[channel][addcommand] = _customtextadd
    
def _create_get_command(channel, customtext):
    getcommand = customtext
    @CustomCommand(getcommand)
    def _customtextget(channel, user, args):
        usage = "Usage: {}{} (<id>)".format(config.cmdprefix, customtext)
        if len(args) == 1:
            try:
                entryid = int(args[0])
            except:
                return usage
            text = _textlists[customtext].getEntry(entryid)
            if text == None:
                return "No {} with id {} exists".format(customtext, entryid)
        elif len(args) == 0:
            text = _textlists[customtext].getEntry()
            if text == None:
                return "No {customtext}s available, add some with {prefix}add{customtext} .".format(
                prefix=config.cmdprefix, customtext=customtext)
        else:
            return usage
        return "{} {} (id: {})".format(utils.at_user(user), text[1], text[0])
    _customtextget.custom = __name__
    _customtextget.__name__ = customtext
    controller._commands[channel][getcommand] = _customtextget
    
def _create_edit_command(channel, customtext):
    editcommand = "edit"+customtext
    @CustomCommand(editcommand, required_permission=permissions.MOD)
    def _customtextedit(channel, user, args):
        usage = "Usage: {}edit{} <id> <new message>".format(config.cmdprefix, customtext)
        if len(args) < 2:
            return usage
        try:
            entryid = int(args[0])
        except:
            return usage
        if _textlists[customtext].editEntry(entryid, " ".join(args[1:])):
            return "Changed {} with id {}.".format(customtext, entryid)
        else:
            return "No {} with id {} exists.".format(customtext, entryid)
    _customtextedit.custom = __name__
    _customtextedit.__name__ = editcommand
    controller._commands[channel][editcommand] = _customtextedit
    
def _create_del_command(channel, customtext):
    delcommand = "del"+customtext
    @CustomCommand(delcommand, required_permission=permissions.MOD)
    def _customtextdel(channel, user, args):
        usage = "Usage: {}del{} <id>".format(config.cmdprefix, customtext)
        if len(args) != 1:
            return usage
        try:
            entryid = int(args[0])
        except:
            return usage
        if _textlists[customtext].deleteEntry(entryid):
            return "Removed {} with id {}.".format(customtext, entryid)
        else:
            return "No {} with id {} exists.".format(customtext, entryid)
    _customtextdel.custom = __name__
    _customtextdel.__name__ = delcommand
    controller._commands[channel][delcommand] = _customtextdel
    
def _del_customtext(channel, customtext):
    commands = ["add"+customtext, customtext, "del"+customtext, "edit"+customtext]
    for cmd in commands:
        del controller._commands[channel][cmd]
    
@Command(required_permission=permissions.MOD)
def addcustomtext(channel, user, args):
    if len(args) != 1:
        return "Usage: {}addcustomtext <type> || recommended to use singlar form".format(config.cmdprefix)
    customtext = args[0]
    if not database[channel].add((True,), (customtext,)):
        return "Custom text {} already exists".format(customtext)
	
    _add_customtext(channel, customtext)
    
    return "Added {customtext} as customtext. New commands: \
    {prefix}{customtext} {prefix}add{customtext} {prefix}del{customtext} \
    {prefix}edit{customtext}".format(customtext=customtext, prefix=config.cmdprefix)
    
@Command(required_permission=permissions.MOD)
def delcustomtext(channel, user, args):
    if len(args) != 1:
        return "Usage: {}delcustomtext <type>".format(config.cmdprefix)
    customtext = args[0]
    if not database[channel].delete((True,), (customtext,)):
        return "Custom text {} does not exists".format(customtext)
    
    _del_customtext(channel, customtext)
    
    return "Deleted {} as customtext.".format(customtext)
    
