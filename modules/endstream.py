from common import permissions, config, utils
from common.textlist import SimpleTextlist
from common.decorator import Command

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']

_instances = {}

class Endstream:

    def __init__(self, channel):
        self.channel = channel
        self.textlist = SimpleTextlist("endstream_{}".format(self.channel[1:]))

    def add(self, message):
        return self.textlist.addEntry(message)

    def delete(self, entryid):
        if self.textlist.deleteEntry(entryid):
            return "Deleted message with id {}.".format(entryid)
        else:
            return "Message with id {} does not exist.".format(entryid)

    def edit(self, entryid, message):
        if self.textlist.editEntry(entryid, message):
            return "Edited message with id {}.".format(entryid)
        else:
            return "Message with id {} does not exist.".format(entryid)

    def get(self, entryid):
        return self.textlist.getEntry(entryid)

    def getall(self):
        return [list(x) for x in self.textlist.getall()]


def init(channel):
    _instances[channel] = Endstream(channel)

@Command(required_permission=permissions.OWNER)
def endstream(channel, user, args):
    messages = _instances[channel].getall()
    if len(args) == 1:
        for idmsg in messages:
            idmsg[1] = idmsg[1].format(channel="https://www.twitch.tv/{}".format(args[0]))
    else:
        messages = [x for x in messages if "{channel}" not in x[1]]
    messages = ["{}".format(x[1]) for x in messages]
    return tuple(messages)

def _add(channel, user, args):
    usage = "{}endconfig add <message>".format(config.cmdprefix)
    message = " ".join(args[1:])
    if message == "":
        return usage
    entryid = _instances[channel].add(message)
    return "Added message with id {}.".format(entryid)

def _del(channel, user, args):
    usage = "{}endconfig del <entryid>".format(config.cmdprefix)
    try:
        entryid = int(args[1])
    except:
        return usage
    return _instances[channel].delete(entryid)


def _edit(channel, user, args):
    usage = "{}endconfig edit <entryid> <message>".format(config.cmdprefix)
    if len(args) < 3:
        return usage
    try:
        entryid = int(args[1])
    except:
        return usage
    message  = " ".join(args[2:])
    return _instances[channel].edit(entryid, message)

def _get(channel, user, args):
    usage = "{}endconfig get (<entryid>)".format(config.cmdprefix)
    if len(args) == 1:
        allmsg = _instances[channel].getall()
        return tuple(["{} (id {})".format(x[1], x[0]) for x in allmsg])
    elif len(args) == 2:
        try:
            entryid = int(args[1])
        except:
            return usage
        return "{}".format(_instances[channel].get(entryid)[1])
    return usage

@Command(required_permission=permissions.OWNER)
def endconfig(channel, user, args):
    usage = "Usage: !endconfig add|del|edit|get"
    if len(args) == 0:
        return usage
    name = "_"+args[0]
    if name in globals().keys():
        return globals()[name](channel,user,args)
    else:
        return usage
