from common import config, utils, permissions, usertracker, genericdatabase
from common.decorator import Command, CustomCommand
from modules import controller

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']

database = {}


def init(channel):
    database[channel] = genericdatabase.GenericDatabase("res/alias.db", 
    channel[1:], [["alias", "TEXT", "PRIMARY KEY"], ["command", "TEXT"], ["command_args", "TEXT"]])
    entries = database[channel].getall()
    for entry in entries:
        _addalias(channel, entry[0], entry[1], entry[2].split(' ') if entry[2] else [])


def getall(channel):
    try:
        return database[channel].getall()
    except KeyError as e:
        return None


def _addalias(channel, alias, command, commandargs):
    @CustomCommand(command)
    def customf(channel, user, args):
        print(user, args)
        if not command in controller._commands[channel]:
            return "The base command {} was deactivated or removed. \
                Remove the alias with {}delalias {}".format(command, 
                    config.cmdprefix, alias)
        return controller._commands[channel][command](channel, user, 
            commandargs + args, False)
    utils.setup_custom_command(channel, customf, alias, __name__)


@Command(required_permission=permissions.MOD)
def addalias(channel, user, args):
    usage = "Usage: {}addalias <aliasname> <old command> (<arguments>)".format(config.cmdprefix)
    if len(args) < 2:
        return usage
    alias = utils.strip_cmdprefix(args[0].lower())
    if alias in controller._commands[channel]:
        return "A command with the name {} already exists.".format(alias)
    command = utils.strip_cmdprefix(args[1].lower())
    if not command in controller._commands[channel]:
        return "{} Command {} does not exist.".format(utils.at_user(user), command)
    commandargs = args[2:]
    if not database[channel].add((True, False, False), 
        (alias, command, " ".join(commandargs))):
        return "Alias {} already exist.".format(alias)
    _addalias(channel, alias, command, commandargs)
    return "Added alias."


@Command(required_permission=permissions.MOD)
def delalias(channel, user, args):
    if len(args) != 1:
        return "Usage: {}delalias <aliasname>".format(config.cmdprefix)
    # if exists blablabla
    alias = args[0]
    if database[channel].delete((True, False, False), (alias, None, None)):
        if alias in controller._commands[channel] \
           and hasattr(controller._commands[channel][alias], "custom") \
           and controller._commands[channel][alias].custom == __name__:
            del controller._commands[channel][alias]
        return "Alias {} deleted.".format(alias)
    else:
        return "Alias {} does not exist.".format(alias)

