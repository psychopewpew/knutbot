import random
from common import config, permissions, usertracker
from common.utils import clamp
from common.decorator import Command

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']

@Command()
def ninja(channel, user, args):
    winchance = config.moduleconfig.get("ninja",channel,"successchance")
    wintimeout = config.moduleconfig.get("ninja",channel,"successtimeout")
    failtimeout = config.moduleconfig.get("ninja",channel,"failtimeout")
    usage = ("Usage: {}ninja <user>".format(config.cmdprefix), "Try to timeout the user for {} seconds. If it fails you get timed out for {} seconds instead. ({:.1f}% chance of success).".format(wintimeout, failtimeout, winchance * 100))
    if len(args) != 1:
        return usage
    success = random.random() < winchance

    result=[]
    targetuser = usertracker.gettracker(channel).get(args[0])
    if success:
        result.append("/timeout {} {}".format(args[0], wintimeout))
        result.append("/me {} successfully sneaks up on {} and knocks them out.".format(user, targetuser))
        if user.color:
            result.insert(0, "/color {}".format(user.color))
    else:
        result.append("/timeout {} {}".format(user, failtimeout))
        result.append("/me {} tries to knock out {} but horribly fails.".format(user, targetuser))
        if targetuser.color:
            result.insert(0, "/color {}".format(targetuser.color))
    return result

@Command(required_permission=permissions.OWNER)
def ninjaconfig(channel, user, args):
    usage = "Usage: {}ninjaconfig get|set successchance|successtimeout|failtimeout <newvalue>".format(config.cmdprefix)
    if len(args) < 1:
        return usage
    if args[0] == "get":
        try:
            return "@{} value: {}".format(user, config.moduleconfig.get("ninja", channel, args[1]))
        except:
            return "Usage: {}ninjaconfig get successchance|successtimeout|failtimeout".format(config.cmdprefix)
    elif args[0] == "set":
        if args[1] == "successtimeout" or args[1] == "failtimeout":
            try:
                time = int(args[2])
                config.moduleconfig.set(time, "ninja", channel, args[1])
                return "Successfully set the value for {} to {}.".format(args[1], time)
            except:
                return "Usage: {}ninjaconfig set {} <seconds>".format(config.cmdprefix, args[1])
        elif args[1] == "successchance":
            try:
                chance = clamp(float(args[2]), 0, 1)
                config.moduleconfig.set(chance, "ninja", channel, args[1])
                return "Successfully set the value for {} to {} ({}%)".format(args[1], chance, chance*100)
            except:
                return "Usage {}nijaconfig set {} <chance (0.0 - 1.0)>".format(config.cmdprefix, args[1])
        else:
            return "Usage: {}ninjaconfig set successchance|successtimeout|failtimeout <newvalue>".format(config.cmdprefix)
    else:
        return usage


def init(channel):
    if not config.moduleconfig.has("ninja", channel, "successchance"):
        config.moduleconfig.set(0.25, "ninja", channel, "successchance")
    if not config.moduleconfig.has("ninja", channel, "successtimeout"):
        config.moduleconfig.set(60, "ninja", channel, "successtimeout")
    if not config.moduleconfig.has("ninja", channel, "failtimeout"):
        config.moduleconfig.set(300, "ninja", channel, "failtimeout")
