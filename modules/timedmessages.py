import threading
import queue

from common import permissions, config, utils
from common.textlist import SimpleTextlist
from common.decorator import Command

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command'] | config.triggertype["message"] | config.triggertype["time"]

_instances = {}

class TimedMessage():

    def __init__(self, channel):
        self.channel = channel
        self.textlist = SimpleTextlist("timedmsges_{}".format(self.channel[1:]))
        self.ids = self.textlist.getIds()
        self.curridx = 0
        self.recentmsgs = 0
        self.timepassed = False
        self.queue = queue.Queue()
        self.waitmsgevent = threading.Event()
        self.timerthread = None
        
        self._init_intervals()

    def _init_intervals(self):
        if not config.moduleconfig.has("timedmessages", self.channel, "minutes between sending"):
            self.set_time_interval(-1)
        if not config.moduleconfig.has("timedmessages", self.channel, "messages between sending"):
            self.set_msg_interval(-1)
        time = float(config.moduleconfig.get("timedmessages", self.channel, "minutes between sending"))
        msg = config.moduleconfig.get("timedmessages", self.channel, "messages between sending")
        self.set_msg_interval(msg)
        self.set_time_interval(time)

    def _message_timer(self, textlist):
        if self.msginterval > 0:
            self.waitmsgevent.wait()
        self.queuemsg(textlist)

    def _message_loop(self):
        textlist = SimpleTextlist("timedmsges_{}".format(self.channel[1:]))
        while not config.exit_event.is_set():
            self._message_timer(textlist)
            config.exit_event.wait(self.timeinterval*60)

    def _quit(self):
        # Fallback method to set self.waitmsgevent on quit
        # _message_timer might get stuck otherwise
        config.exit_event.wait()
        self.waitmsgevent.set()

    def add(self, message):
        newid = self.textlist.addEntry(message)
        self.ids.append(newid)
        if len(self.ids) > 0:
            self.starttimer()
        return "Added message with id {}".format(newid)

    def delete(self, entryid):
        if self.textlist.deleteEntry(entryid):
            idx = self.ids.index(entryid)
            if idx < self.curridx:
                self.curridx -= 1
            elif idx == self.curridx:
                if idx == len(self.ids) - 1:
                    self.curridx = 0
            self.ids.remove(entryid)
            return "Delted entry with id {}".format(entryid)
        else:
            return "Could not delete, entry with id {} does not exist".format(entryid)

    def edit(self, editid, message):
        if self.textlist.editEntry(editid, message):
            return "Edited entry with id {}".format(editid)
        else:
            return "Could not edit, entry with id {} does not exist".format(editid)

    def get(self, entryid):
        entry = self.textlist.getEntry(entryid)
        if entry == None:
            return "Entry with id {} does not exist.".format(entryid)
        else:
            return "{}".format(entry[1])

    def get_queue_message(self):
        try:
            return self.queue.get(block=False)
        except queue.Empty:
            return None

    def is_setup(self):
        return self.timeinterval > 0 or self.msginterval > 0

    def queuemsg(self, textlist):
        if len(self.ids) > 0:
            entry = textlist.getEntry(entryid=self.ids[self.curridx])
            self.queue.put("{} (id: {})".format(entry[1], entry[0]))
            self.curridx = (self.curridx + 1) % len(self.ids)
            config.timedmsg_event.set()
        self.recentmsgs = 0
        self.waitmsgevent.clear()

    def set_msg_interval(self, msg):
        config.moduleconfig.set(msg,"timedmessages", self.channel, "messages between sending")
        self.msginterval = msg
        return "Set message interval to {} messages".format(self.msginterval)


    def set_time_interval(self, time):
        modified = False
        if 0 <= time and time < 1 and self.msginterval <= 0:
            time = 1
            modified = True
        config.moduleconfig.set(time,"timedmessages", self.channel, "minutes between sending")
        self.timeinterval = time
        self.starttimer()
        return "Set time interval to {} minutes{}".format(self.timeinterval, ", because message interval is set to 0 (1 minute minimum in that case)" if modified else "")

    def starttimer(self):
        if self.timerthread != None or self.timeinterval < 0:
            return
        self.timerthread = threading.Thread(target=self._message_loop)
        self.timerthread.start()
        threading.Thread(target=self._quit).start()


    def trigger(self):
        if not self.waitmsgevent.is_set():
            self.recentmsgs += 1
            if self.recentmsgs >= self.msginterval:
                self.waitmsgevent.set()

def _add(channel, user, args):
    if len(args) < 2:
        return "Usage: {}timedmsg add <message>".format(config.cmdprefix)
    return _instances[channel].add(' '.join(args[1:]))

def _del(channel, user, args):
    try:
        if len(args) != 2:
            raise Exception
        delid = int(args[1])
    except:
        return "Usage: {}timedmsg del <id>".format(config.cmdprefix)
    return _instances[channel].delete(delid)

def _edit(channel, user, args):
    usage = "Usage: {}timedmsg edit <id> <new message>".format(config.cmdprefix)
    if len(args) < 3:
        return usage
    try:
        editid = int(args[1])
        msg = ' '.join(args[2:])
    except:
        return usage

    return _instances[channel].edit(editid, msg)

def _get(channel, user, args):
    usage = "Usage: {}timedmsg del <id>".format(config.cmdprefix)
    if len(args) != 2:
        return usage
    try:
        entryid = int(args[1])
    except:
        return usage
    return _instances[channel].get(entryid)

def _getids(channel, user, args):
    return ", ".join(["{}".format(x) for x in _instances[channel].ids])

def _getintervals(channel, user, args):
    usage = "Usage: {}timedmsg getintervals".format(config.cmdprefix)
    if len(args) != 1:
        return usage
    return "{} minutes and {} messages between every send message (both conditions need to be met)".format(_instances[channel].timeinterval, _instances[channel].msginterval)

def _setinterval(channel, user, args):
    usage = "Usage: {0}timedmsg setinterval time <minutes> | {0}timedmsg setinterval messages <amount>".format(config.cmdprefix)
    if len(args) != 3:
        return usage
    if args[1] == "time":
        try:
            time = float(args[2])
        except:
            return usage
        return _instances[channel].set_time_interval(time)
    elif args[1] == "messages":
        try:
            msgs = int(args[2])
        except:
            return usage
        return _instances[channel].set_msg_interval(msgs)
    else:
        return usage

def collect_time_messages(channel):
    return _instances[channel].get_queue_message()

def init(channel):
    _instances[channel] = TimedMessage(channel)

def notice_on_activate(channel):
    if not _instances[channel].is_setup():
        return "To have messages displayed you need to set either time or message interval (or both)."

@Command(required_permission=permissions.OWNER)
def timedmsg(channel, user, args):
    usage = (
        "Usage: {0}timedmsg add <message> | {0}timedmsg edit <id> <new message> | {0}timedmsg delete <id> | {0}timedmsg get <id> | {0}timedmsg getids".format(config.cmdprefix),
        "{0}timedmsg getintervals | {0}timedmsg setinterval time <minutes> | {0}timedmsg setinterval messages <amount>".format(config.cmdprefix)
        )
    if len(args) == 0:
        return usage
    name = "_"+args[0]
    if name in globals().keys():
        return globals()[name](channel,user,args)
    else:
        return usage


def trigger(channel, user, message, is_command):
    if not is_command:
        _instances[channel].trigger()
