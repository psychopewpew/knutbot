from common import config, permissions, usertracker
from common.decorator import Command

moduletype = config.moduletype["remote"] | config.moduletype["local"]
triggertype = config.triggertype['command']

@Command(required_permission=permissions.OWNER)
def setpermission(channel, user, args):
    usage = "Usage: {}setpermissions <user> <level>".format(config.cmdprefix),"Available levels: owner, mod, everyone"
    if len(args)  != 2:
        return usage
    level = args[1].upper()
    if hasattr(permissions, level):
        levelobject = getattr(permissions, level)
        if levelobject > permissions.OWNER:
            return usage
        target = args[0].lower()
        if target == channel[1:]:
            return "Cannot set permission for channel owner."
        usertracker.gettracker(channel).update_permission(target, levelobject)
        return "Set permission to {} for {}".format(levelobject, args[0])
    else:
        return usage

@Command(required_permission=permissions.OWNER)
def listpermission(channel, user, args):
    usage = "Usage: {}listpermission".format(config.cmdprefix)
    if len(args) != 0:
        return usage
    owners = map(lambda u: u.displayname, usertracker.gettracker(channel).getowners())
    mods = map(lambda u: u.displayname, usertracker.gettracker(channel).getmods())
    return "Owner: {}".format(", ".join(owners)), "Mod: {}".format(", ".join(mods))
