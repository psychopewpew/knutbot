from common import utils, config, permissions, stopwatchtracker
from common.decorator import Command
import datetime

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command']

_instances = {}

class QATimeTracker():

    def __init__(self, channel):
        self.tracker = stopwatchtracker.StopwatchTracker("qa_{}".format(channel[1:]), channel)
        self.channel = channel
        self.question = None

    def start(self, user, message):
        # Send question to user that asked question for  stopping timer
        if self.tracker.start():
            self.question = message
            return "@{} Type !timea when the question was answered to stop the timer.".format(user), "@{} Timed Question: {}".format(self.channel[1:], self.question)
        else:
            return "Already running: {} @{}".format(self.question, self.channel[1:])

    def stop(self):
        diff = self.tracker.stop()
        if diff == -1:
            return "No question is running right now."
        else:
            if diff > 60:
                minutes = int(diff / 60)
                seconds = diff - minutes * 60
                return "Response time: {:d} minutes {:.2f} seconds".format(minutes, seconds)
            else:
                return "Response time: {:.2f} seconds".format(diff)

    def calculate(self, function, start, stop):
        result = self.tracker.calculate(function, start, stop)
        if result.count == 0:
            return "No timed questions found in the given timeframe."
        else:
            if result.value > 60:
                minutes = int(result.value / 60)
                seconds = result.value - minutes * 60
                return "The {} response time was {:d} minutes {:.2f} seconds (with {} questions asked)".format(function.lower(), minutes, seconds, result.count)
            return "The {} response time was {:.2f} seconds (with {} questions asked)".format(function.lower(), result.value, result.count)


def init(channel):
    _instances[channel] = QATimeTracker(channel)

@Command()
def timeq(channel, user, args):
    if len(args) == 0:
        return "Usage: {} !timeq <question>".format(config.cmdprefix)
    return _instances[channel].start(user, " ".join(args))

@Command()
def timea(channel, user, args):
    return _instances[channel].stop()

@Command()
def timeavg(channel, user, args):
    return timecalc(channel, "AVG", args)

@Command()
def timemin(channel, user, args):
    return timecalc(channel, "MIN", args)

@Command()
def timemax(channel, user, args):
    return timecalc(channel, "MAX", args)

def timecalc(channel, function, args):
    usage = "Usage: {}timeavg <(start)date> (<enddate>) | dates are in the format YYYY.MM.DD (ex. 2018.01.01)".format(config.cmdprefix)
    if len(args) == 0 or len(args) > 2:
        return usage
    try:
        start = datetime.datetime.strptime(args[0], "%Y.%m.%d").timestamp()
        stop = (datetime.datetime.strptime(args[0] if len(args) == 1 else args[1], "%Y.%m.%d") + datetime.timedelta(days=1)).timestamp()
        return _instances[channel].calculate(function, start, stop)
    except ValueError as e:
        return usage
    except:
        raise
