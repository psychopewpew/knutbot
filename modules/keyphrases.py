from common import config, utils, permissions, usertracker, genericdatabase
from common.decorator import Command
from modules import controller
import time

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command'] | config.triggertype['message']

_instances = {}

_FILENAME = "res/keyphrases.db"

_PUNCTUATION = '.:",;!?'
_TRANS_TABLE = str.maketrans(_PUNCTUATION, ' ' * len(_PUNCTUATION))

class KeyPhraseTrigger(object):

    def __init__(self, channel):
        self.db = genericdatabase.GenericDatabase(
            _FILENAME,
            channel[1:],
            [["keyphrase", "TEXT", "PRIMARY KEY"],
             ["response", "TEXT"],
             ["cooldown_seconds", "INTEGER"]])
        self.channel = channel
        self.last_triggered = {}

    def add(self, user, key, response):
        check = self.check_response_cmdpermission(user, response)
        if check:
            return check
        transkey = key.translate(_TRANS_TABLE)
        if self.db.add((True, False, False), (transkey, response, 0)):
            return "Added keyphrase '{}'".format(transkey)
        else:
            return "Keyphrase '{}' already exists.".format(transkey)

    def check_response_cmdpermission(self, user, response):
        splits = map(lambda s: s.strip(), response.split('\\n'))
        for split in splits:
            if split.startswith(config.cmdprefix):
                command = split.split(' ')[0][len(config.cmdprefix):]
                if command not in controller._commands[self.channel]:
                    return "Command {} does not exist. Will not add keyphrase".format(command)
                req_perm = controller._commands[self.channel][command].required_permission
                # make it so one cannot bypass permissions with keywords
                # the person adding a keyphrase needs to be allowed 
                # to use the command
                if req_perm > user.permission:
                    return "You can only use commands as response that you can\
                        execute (have the required permission)."
        return None

    def delete(self, key):
        if self.db.delete((True, False), (key, None)):
            return "Keyphrase '{}' deleted.".format(key)
        else:
            return "Keyphrase '{}' doesn't exist.".format(key)

    def edit(self, user, key, newresponse):
        check = self.check_response_cmdpermission(user, newresponse)
        if check:
            return check
        if self.db.edit((True, False, False), (key, None, None),
                        (False, True, False), (None, newresponse, None)):
            return "Keyphrase '{}' edited.".format(key)
        else:
            return "Keyphrase '{}' doesn't exist.".format(key)

    def getall(self):
        return self.db.getall()

    def raw(self, key):
        response = self.db.get((True, False, False), (key, None, None))
        if response is None:
            return "Keyphrase '{}' doesn't exist.".format(key)
        return "Raw: " + response[1]

    def set_cooldown(self, key, cooldown_seconds):
        if self.db.edit((True, False, False), (key, None, None),
                        (False, False, True), (None, None, cooldown_seconds)):
            return "Set cooldown for '{}' to {} seconds".format(key, cooldown_seconds)
        else:
            return "Keyphrase '{}' doesn't exist.".format(key)

    def trigger(self, user, message):
        lowermessage = " ".join(message).lower().translate(_TRANS_TABLE)
        key_responses = self.db.getall()
        if not key_responses:
            return None

        fullresponse = []
        for key, response, cooldown in key_responses:
            if cooldown > 0 and key in self.last_triggered and \
                    self.last_triggered[key] + cooldown > time.time():
                continue
            start = lowermessage.find(key)
            if start == -1:
                continue
            # check if key is surrounded by whitespace
            # => do not match keyphrase "night" in "knight"
            left = max(0, start-1)
            right = start+len(key)+1
            messagekey = lowermessage[left:right].strip()
            # if surrounded by whitespace it equals the key now
            if key == messagekey:
                formatted_responses = map(lambda s: s.strip(),
                                          response.format(user=user).split('\\n'))
                for fr in formatted_responses:
                    if fr.startswith(config.cmdprefix):
                        # always execute it as broadcaster, so no permission
                        # issues arise it is checked when adding wether the
                        # person can execute the command
                        broadcaster = usertracker.gettracker(self.channel).get(
                            self.channel[1:])
                        split_r = fr.split(' ')
                        command = split_r[0][len(config.cmdprefix):]
                        if command in controller._commands[self.channel]:
                            result = controller._commands[self.channel][command](
                                self.channel, broadcaster, split_r[1:], False)
                            if result:
                                fullresponse.extend(result)
                        else:
                            return "Command {} that was set as keyphrase response \
                                does'nt exist anymore.".format(command)
                    else:
                        fullresponse.append(fr)
                self.last_triggered[key] = time.time()
        return fullresponse


def init(channel):
    _instances[channel] = KeyPhraseTrigger(channel)


def trigger(channel, user, message, is_command):
    if not is_command:
        return _instances[channel].trigger(user, message)


def _verify_new_keyphrase_args(command, args):
    if len(args) == 0:
        return "Usage: {}{} \"<keyphrase>\" <response> || for formatting \
            options check: {}modules/keyphrases".format(config.cmdprefix,
                                                        command,
                                                        config.WIKI_URL)
    if not args[0].startswith('"'):
        return "You need to surround the keyphrase with \"\" like \"KEYPHRASE\"."
    return None


def _parse_new_keyphrase(args):
    n = 0
    while n < len(args):
        if args[n].endswith('"'):
            break
        n += 1
    # get args entries that belong to the keyphrase: args[0:n]
    # join them into a single string and then remove the ""
    keyphrase = " ".join(args[0:n+1])[1:-1].lower()
    response = " ".join(args[n+1:])
    return keyphrase, response

def getall(channel):
    try:
        return _instances[channel].getall()
    except KeyError as e:
        return None

@Command(required_permission=permissions.MOD)
def addkeyphrase(channel, user, args):
    verify_response = _verify_new_keyphrase_args("addkeyphrase", args)
    if verify_response:
        return verify_response
    keyphrase, response = _parse_new_keyphrase(args)
    if not response:
        return "The response cannot be empty!"
    return _instances[channel].add(user, keyphrase, response)


@Command(required_permission=permissions.MOD)
def cdkeyphrase(channel, user, args):
    if len(args) == 0:
        return "Usage: {}cdkeyphrase \"<keyphrase>\" <cooldown in seconds>"
    if not args[0].startswith('"'):
        return "You need to surround the keyphrase with \"\" like \"KEYPHRASE\"."
    keyphrase, response = _parse_new_keyphrase(args)
    try:
        cooldown = int(response)
    except ValueError:
        return "The <cooldown in seconds> needs to be an integer."
    return _instances[channel].set_cooldown(keyphrase, cooldown)

@Command(required_permission=permissions.MOD)
def delkeyphrase(channel, user, args):
    if len(args) == 0:
        return "Usage: {}delkeyphrase <key phrase>".format(config.cmdprefix)
    key = " ".join(args)
    if key.startswith('"'):
        key = key[1:-1]
    return _instances[channel].delete(key)


@Command(required_permission=permissions.MOD)
def editkeyphrase(channel, user, args):
    verify_response = _verify_new_keyphrase_args("addkeyphrase", args)
    if verify_response:
        return verify_response
    keyphrase, response = _parse_new_keyphrase(args)
    if not response:
        return "The response cannot be empty!"
    return _instances[channel].edit(user, keyphrase, response)


@Command(required_permission=permissions.MOD)
def rawkeyphrase(channel, user, args):
    if len(args) == 0:
        return "Usage: {}rawkeyphrase <key phrase>".format(config.cmdprefix)
    key = " ".join(args)
    if key.startswith('"'):
        key = key[1:-1]
    return _instances[channel].raw(key)
