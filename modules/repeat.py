from common import config, permissions, usertracker
from common.decorator import Command

moduletype = config.moduletype["remote"]
triggertype = config.triggertype['command'] | config.triggertype['message']

_instances = {}


@Command()
def repeat(channel, user, args):
    try:
        v = _instances[channel][user]
        v[1] += 1
        s = "{0[0]} ({0[1]})".format(v)
        return s
    except KeyError:
        pass


def init(channel):
    _instances[channel] = {}


def trigger(channel, user, message, is_command):
    if not is_command:
        _instances[channel][user] = [" ".join(message), 1]
