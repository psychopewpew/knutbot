from functools import total_ordering
import sys

@total_ordering
class Permissionlevel():

    def __init__(self, alias, level):
        self.alias = alias
        self.level = level

    def __str__(self):
        return self.alias

    def __repr__(self):
        return "Permissionlevel[Alias={}, Level={}]".format(self.alias, self.level)

    def __eq__(self, other):
        if hasattr(other, "level"):
            return self.level == other.level
        if isinstance(other, int):
            return self.level == other
        return False

    def __lt__(self, other):
        if hasattr(other, "level"):
            return self.level < other.level
        if isinstance(other, int):
            return self.level < other
        return False

    def _is_valid_operand(self, other):
        return


EVERYONE = Permissionlevel("Everyone", 0)
VIP = Permissionlevel("VIP", 25)
MOD = Permissionlevel("Mod", 50)
OWNER = Permissionlevel("Owner", 100)
ADMIN = Permissionlevel("Admin", 150)

permdict = {
    EVERYONE.level: EVERYONE,
    VIP.level: VIP,
    MOD.level: MOD,
    OWNER.level: OWNER,
    ADMIN.level: ADMIN
}
