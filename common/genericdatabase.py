import sqlite3
import threading


class GenericDatabase():

    def __init__(self, filename, tablename, columns):
        """
        The parameter columns must be a list of lists with
        [columnname, columntype, additional-specs] where additional-specs
        can be omitted.
        """
        self._filename = filename
        self._tablename = tablename
        self._connections = {}
        self._cursors = {}

        self.columnlen = len(columns)
        self.columnnames = [s[0] for s in columns]
        self.columntypes = [s[1] for s in columns]
        self.columnadditions = [s[2] if len(s) > 2 else "" for s in columns]

        columndesc = [" ".join(s) for s in columns]

        self.cursor.execute("CREATE TABLE IF NOT EXISTS {} ({})".format(
            self._tablename, ", ".join(columndesc)))

    @property
    def connection(self):
        tid = threading.get_ident()
        if tid not in self._connections:
            self._connections[tid] = sqlite3.connect(self._filename)
            # uncomment if debugging queries
            # self._connections[tid].set_trace_callback(print)
        return self._connections[tid]

    @property
    def cursor(self):
        tid = threading.get_ident()
        if tid not in self._cursors:
            self._cursors[tid] = self.connection.cursor()
        return self._cursors[tid]

    def add(self, existmask, values, commit=True):
        if self.exists(existmask, values):
            return False
        self.cursor.execute("INSERT INTO {} VALUES ({})".format( \
        self._tablename, ",".join(["?"]*self.columnlen)), values)
        if commit:
            self.connection.commit()
        return True
        
    def clear(self):
        self.cursor.execute("DELETE FROM {}".format(self._tablename))
        self.connection.commit()

    def delete(self, existmask, values):
        if not self.exists(existmask, values):
            return False
        checklist, valuetuple = self.parse_mask_values(existmask, values)
        self.cursor.execute("DELETE FROM {} WHERE {}".format(
            self._tablename, checklist), valuetuple)
        self.connection.commit()
        return True
        
    def drop(self):
        self.cursor.execute("DROP TABLE {}".format(self._tablename))
        self.connection.commit()

    def edit(self, existmask, values, newmask, newvalues):
        if not self.exists(existmask, values):
            return False
        else:
            oldchecklist, oldvalues = self.parse_mask_values(existmask, values)
            newchecklist, newvaluetuple = self.parse_mask_values(
                newmask, newvalues, joinstr=", ", lowertext=False)
            query = "UPDATE {} SET {} WHERE {}".format(
                self._tablename, newchecklist, oldchecklist)
            self.cursor.execute(query, newvaluetuple + oldvalues)
            self.connection.commit()
            return True
            
    def get(self, existmask, values):
        checklist, valuetuple = self.parse_mask_values(existmask, values)
        self.cursor.execute("SELECT * FROM {} WHERE {}".format(
            self._tablename, checklist),valuetuple)
        return self.cursor.fetchone()

    def exists(self, existmask, values):
        return self.get(existmask, values) is not None

    def parse_mask_values(self, existmask, values, joinstr=" AND ", lowertext=True):
        checklist = []
        valuelist = []
        for mask, columnname, columntype, value in zip(existmask, 
            self.columnnames, self.columntypes, values):
            if mask:
                if columntype == "TEXT" and lowertext:
                    checklist.append("lower({})=lower(?)".format(columnname))
                else:
                    checklist.append("{}=?".format(columnname))
                valuelist.append(value)
        return joinstr.join(checklist), tuple(valuelist)

    def getall(self):
        self.cursor.execute("SELECT * FROM {}".format(self._tablename))
        return self.cursor.fetchall()
