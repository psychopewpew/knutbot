import json
import shutil
import os.path
from . import utils
from threading import Event

class Config():

    def __init__(self, conffile):
        self._conffile = conffile
        self._config = {}

        utils.ensure_file_exists(self._conffile,  "{}")
        self.load()

    def add(self, value, *path):
        if len(path) == 0:
            raise Exception
        currentpoint = self._config
        for i in range(len(path)-1):
            key = path[i]
            if key not in currentpoint:
                currentpoint[key] = {}
            currentpoint = currentpoint[key]

        key = path[-1]
        if key not in currentpoint:
            currentpoint[key] = []
        if value not in currentpoint[key]:
            currentpoint[key].append(value)
            self.save()

    def delete(self, *path):
        if len(path) == 0:
            raise Exception
        currentpoint = self._config
        for i in range(len(path)-1):
            key = path[i]
            currentpoint = currentpoint[key]
        key=path[-1]
        del currentpoint[key]
        self.save()

    def get(self, *path):
        if len(path) == 0:
            raise Exception
        currentpoint = self._config
        for pathentry in path:
            currentpoint = currentpoint[pathentry]
        return currentpoint

    def get_default(self, defaultval, *path):
        if not self.has(*path):
            return defaultval
        else:
            return self.get(*path)

    def has(self, *path):
        if len(path) == 0:
            raise Exception
        currentpoint = self._config
        for pathentry in path:
            if pathentry not in currentpoint:
                return False
            currentpoint = currentpoint[pathentry]
        return True

    def load(self):
        with open(self._conffile,'r') as f:
            self._config = json.loads(' '.join(f.readlines()))

    def remove(self, value, *path):
        if len(path) == 0:
            raise Exception
        currentpoint = self._config
        for i in range(len(path)-1):
            key = path[i]
            currentpoint = currentpoint[key]
        key=path[-1]
        currentpoint[key].remove(value)
        self.save()

    def save(self):
        with open(self._conffile+".new",'w') as f:
            f.write(json.dumps(self._config, sort_keys=True, indent=4, separators=(',', ': ')))
        shutil.move(self._conffile+".new", self._conffile)

    def set(self, value, *path):
        if len(path) == 0:
            raise Exception
        currentpoint = self._config
        for i in range(len(path)-1):
            key = path[i]
            if key not in currentpoint:
                currentpoint[key] = {}
            currentpoint = currentpoint[key]

        if path[-1] not in currentpoint or currentpoint[path[-1]] != value:
            currentpoint[path[-1]] = value
        self.save()

    def tostring(self):
        return "{}".format(self._config)

    def __str__(self):
        return str(self._config)


if not os.path.isfile("res/config.json"):
    raise Exception("\nres/config.json must exists to use this bot.\nCheck res/config.json.example to set it up properly.")

globalconfig = Config("res/config.json")
moduleconfig = Config("res/moduleconfig.json")

moduletype = {
    "local" : 0b1,
    "remote" : 0b10,

}

triggertype = {
    "command" : 0b1,
    "message" : 0b10,
    "time"    : 0b100,
}

WIKI_URL = "https://gitlab.com/psychopewpew/knutbot/wikis/"

runtype = None

irc_text_limit = 512-64 # -64 to compensate IRC command stuff

cmdprefix = globalconfig.get("commandprefix")
exit_event = Event()
timedmsg_event = Event()

log_command_stats = False

def get(*path):
    return globalconfig.get(*path)

def get_default(defaultval, *path):
    return globalconfig.get_default(defaultval, *path)

def set(value, *path):
    return globalconfig.set(value, *path)

def add(value, *path):
    return globalconfig.add(value, *path)

def has(*path):
    return globalconfig.has(*path)

def remove(value, *path):
    return globalconfig.remove(value, *path)

def delete(*path):
    return globalconfig.delete(*path)
