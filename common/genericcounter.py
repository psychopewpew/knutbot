from common import numbertracker, config, utils, permissions, usertracker
from common.twitchapi import helper as twitchapi
from common.twitchapi.exceptions import *
import time


    

class GenericCounter():
    
    def __init__(self, channel, noun, verb):
        self.channel = channel
        self.broadcaster = usertracker.gettracker(channel).get(channel[1:])
        self.noun = noun
        self.verb = verb
        self.tracker = numbertracker.NumberTracker("{}_{}".format(self.noun, self.broadcaster.name))
    
    def add(self, gamename=None, count=1):
        if not gamename:
            try:
                gamename = twitchapi.get_gamename(self.broadcaster)
            except (OAuthTokenNotSetUp) as e:
                return str(e)
        game, _, new = self.tracker.add(gamename, count)
        return "{} {} {} times in {}.".format(
            self.broadcaster, self.verb, new, game)
    
    def current(self, gamename=None):
        if not gamename:
            try:
                gamename = twitchapi.get_gamename(self.broadcaster)
            except (OAuthTokenNotSetUp) as e:
                return "Please provide the game name to get the death counter ({})".format(e)
        result = self.tracker.get(gamename)
        return "{} {} {} times in {}.".format(
            self.broadcaster, self.verb, result[1], result[0])
            
    def set(self, count, gamename=None):
        if not gamename:
            try:
                gamename = twitchapi.get_gamename(self.broadcaster)
            except (OAuthTokenNotSetUp) as e:
                return "Please provide the game name to get the death counter ({})".format(e)
        game, old, new = self.tracker.set(gamename, count)
        return "Set {} counter from {} to {} for {}.".format(self.noun, 
            old, new, game)
            
    def clear(self):
        self.tracker.clear()
        return "Cleared all counters for {}".format(self.noun)
            
    def sum(self):
        return "{} {} {} times overall.".format(self.broadcaster, self.verb, self.tracker.sum())

