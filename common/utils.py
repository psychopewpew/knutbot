from pathlib import Path
import distutils.util
import os
import errno
import distutils.util
from common import config

currentcolor = None

def ensure_file_exists(filename, defaultcontent=""):
    filepath = Path(filename)
    if not filepath.exists():
        with open(filename, 'w') as f:
            f.write(defaultcontent)

def ensure_dir_exists(dirpath):
    try:
        os.makedirs(dirpath)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

def at_user(user):
    return "@"+user.displayname

def at_username(username):
    if username[0] == "@":
        return username
    else:
        return "@" + username

def no_at_username(username):
    if username[0] == '@':
        return username[1:]
    else:
        return username

def wrap_in_tuple(entries):
    if entries is None:
        return ()
    if isinstance(entries, str):
        return (entries,)
    try:
        return tuple(entries)
    except:
        return (entries,)

def wrap_in_list(entries):
    if entries is None:
        return []
    if isinstance(entries, str):
        return [entries]
    try:
        return list(entries)
    except:
        return [entries]

def clamp(value, lower, upper):
    return lower if value < lower else upper if value > upper else value

def waitonevent(event, time, loops):
    for _ in range(loops):
        event.wait(time)

def strtobool(s):
    return bool(distutils.util.strtobool(s))

def wrap_and_change_color(channel, entries, drop_colors=False):
    answerlist = wrap_in_list(entries)
    if len(answerlist) == 0:
        return answerlist

    if drop_colors:
        return [a for a in answerlist if not a.startswith("/color")]

    changingcolor = False
    global currentcolor
    newcolor = None
    # Color is being changed by answer
    if answerlist[0].startswith("/color"):
        newcolor = answerlist[0][7:]
        # Remove color change from answer, might not be needed
        answerlist = answerlist[1:]
    # Get color for channel if specified, else default if specified
    elif config.has("colors", channel):
        newcolor = config.get("colors", channel)
    elif config.has("colors", "default"):
        newcolor = config.get("colors", "default")
    # Insert color change command if it's a new color
    # to minimize number of messages sent
    if newcolor and newcolor != currentcolor:
        print("Changing color:", currentcolor, "to", newcolor)
        answerlist.insert(0, "/color {}".format(newcolor))
        currentcolor = newcolor
        changingcolor = True
    # Check if there are any further color change commands,
    # to have correct currentcolor value
    for a in answerlist[2 if changingcolor else 1:]:
        if a.startswith("/color"):
            currentcolor = a[7:]
    return answerlist

def strip_cmdprefix(command):
    if command.startswith(config.cmdprefix):
        command = command[len(config.cmdprefix):]
    return command

def compress_message_list(msglist, join_string=" "):
    tmp = []
    tmplen = 0
    result = []
    for msg in msglist:
        if tmplen + len(msg) > config.irc_text_limit:
            result.append(join_string.join(tmp))
            tmp = []
            tmplen = 0
        tmp.append(msg)
        tmplen += len(msg) + len(join_string)
    result.append(join_string.join(tmp))
    return result

def setup_custom_command(channel, function, command, customtype):
    from modules import controller
    function.custom = customtype
    function.__name__ = command
    controller._commands[channel][command] = function
