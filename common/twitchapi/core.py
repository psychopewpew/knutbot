from abc import ABC, abstractmethod
from common import config
from common.twitchapi.exceptions import OAuthTokenInvalid, OAuthTokenNotSetUp
import requests
import json
import time


class TwitchAPI(object):
    
    VALIDATION_URL = "https://id.twitch.tv/oauth2/validate"
    
    def __init__(self):
        self.token = config.get_default(None, "twitchapi_oauth")
        self.baseurl = self._baseurl()
        self.last_validation = {}

    def get_request_as_json(self, url, parameters, token):
        self.validate(token)
        r = requests.get(url, headers=self.headers(token), params=parameters, timeout=5)
        r.raise_for_status()
        return json.loads(r.text)

    def get(self, url, parameters={}, oauthtoken=None):
        token = oauthtoken if oauthtoken else self.token
        if not token:
            raise OAuthTokenNotSetUp()
        return self.get_request_as_json(self.baseurl + url, parameters, token)

    def valid(self):
        return bool(self.token)

    def validate(self, token):
        currenttime = time.time()
        if token in self.last_validation and \
          self.last_validation[token][0] + 3600 > currenttime:
            return self.last_validation[token][1]
        r = requests.get(self.VALIDATION_URL,
                         headers={"Authorization": "OAuth {}".format(token)},
                         timeout=5)
        r.raise_for_status()
        j = json.loads(r.text)
        if 'message' in j:
            msg = j["message"]
            if msg == "invalid access token":
                raise OAuthTokenInvalid()
        self.last_validation[token] = (currenttime, j)
        return j

    @abstractmethod
    def _baseurl(self):
        pass

    @abstractmethod
    def headers(self, token):
        pass
