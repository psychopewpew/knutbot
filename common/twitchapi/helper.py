from common import config
from common.twitchapi import helix, kraken
from common.twitchapi.exceptions import *

from datetime import datetime, timezone
from dateutil import parser

USER_DICT = {}


def valid():
    return config.has("twitchapi_oauth")


def _user_id(user):
    if user.name not in USER_DICT:
        usersdata = helix.users(logins=[user.name])
        try:
            userid = usersdata["data"][0]["id"]
            USER_DICT[user.name] = userid
            USER_DICT[userid] = user.name
        except IndexError as e:
            raise UserDoesNotExist(user.name)
    return USER_DICT[user.name]


def _user_name(userid):
    if userid not in USER_DICT:
        usersdata = helix.users(logins=[userid])
        username = usersdata["data"][0]["login"]
        USER_DICT[username] = userid
        USER_DICT[userid] = username
    return USER_DICT[userid]


def get_gamename(user):
    userid = _user_id(user)
    channeldata = kraken.channel(userid)
    game_name = channeldata["game"]
    return game_name


def get_room_ids(user):
    userid = _user_id(user)
    rooms = kraken.chat_rooms_by_channel(userid)
    ids = list(map(lambda d: d["_id"], rooms["rooms"]))
    return ids


def get_userid(user):
    return _user_id(user)


def get_username(userid):
    return _user_name(userid)


def username(oauthtoken):
    data = helix._instance.validate(oauthtoken)
    return data['login']


def uptime(user):
    userid = _user_id(user)
    try:
        data = helix.streams(ids=[userid])["data"][0]
    except IndexError as e:
        raise UserNotLive(user.displayname)
    starttime = parser.parse(data["started_at"])
    now = datetime.now(timezone.utc)
    uptimeseconds = (now - starttime).seconds
    hours = uptimeseconds // 3600
    minutes = (uptimeseconds % 3600) // 60
    seconds = uptimeseconds % 60
    return hours, minutes, seconds


def followage(fromuser, touser):
    fromid = _user_id(fromuser)
    toid = _user_id(touser)
    try:
        data = helix.users_follows(from_id=fromid, to_id=toid)["data"][0]
    except IndexError as e:
        raise NotFollowing(fromuser.displayname, touser.displayname)
    return parser.parse(data["followed_at"])








