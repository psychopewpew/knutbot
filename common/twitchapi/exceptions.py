class OAuthTokenNotSetUp(Exception):
    def __str__(self):
        return "OAuth Token for Twitch API not set up"


class OAuthTokenInvalid(Exception):
    def __str__(self):
        return "The OAuth token was invalidated. Please request a new one."


class UserDoesNotExist(Exception):

    def __init__(self, username):
        self.username = username

    def __str__(self):
        return f"User {self.username} does not exist."


class UserNotLive(Exception):

    def __init__(self, username):
        self.username = username

    def __str__(self):
        return f"User {self.username} is not live."


class NotFollowing(Exception):

    def __init__(self, from_name, to_name):
        self.from_name = from_name
        self.to_name = to_name

    def __str__(self):
        return f"{self.from_name} does not follow {self.to_name}."
