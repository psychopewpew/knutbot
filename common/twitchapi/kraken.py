from common import config
from common.twitchapi.core import TwitchAPI
from common.twitchapi.exceptions import *

class Kraken(TwitchAPI):

    def headers(self, token):
        return {
            "Authorization": "OAuth {}".format(token),
            'Accept': 'application/vnd.twitchtv.v5+json'
                }

    def _baseurl(self):
        return "https://api.twitch.tv/kraken/"


_instance = Kraken()


def chat_rooms_by_channel(channel_id):
    return _instance.get("chat/{}/rooms".format(channel_id))


def channel(channel_id):
    return _instance.get("channels/{}".format(channel_id))
