from common import config
from common.twitchapi.core import TwitchAPI
from common.twitchapi.exceptions import *


class Helix(TwitchAPI):

    def headers(self, token):
        return {
            "Authorization": "Bearer {}".format(token),
            "Client-ID": config.get("client_id"),
            'Accept': 'application/vnd.twitchtv.v5+json'
                }

    def _baseurl(self):
        return "https://api.twitch.tv/helix/"


_instance = Helix()


def users(ids=[], logins=[], oauthtoken=None):
    parameters = {}
    if ids:
        parameters["id"] = ids
    if logins:
        parameters["login"] = logins
    return _instance.get("users", parameters, oauthtoken)


def streams(ids=[], logins=[], oauthtoken=None):
    parameters = {}
    if ids:
        parameters["user_id"] = ids
    if logins:
        parameters["user_login"] = logins
    return _instance.get("streams", parameters, oauthtoken)


def users_follows(from_id="", to_id="", oauthtoken=None):
    parameters = {}
    if from_id:
        parameters["from_id"] = from_id
    if to_id:
        parameters["to_id"] = to_id
    return _instance.get("users/follows", parameters, oauthtoken)
