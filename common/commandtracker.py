import sqlite3
import time

class CommandTracker():

    def __init__(self):
        self._connection = sqlite3.connect("res/commandstats.db")
        self._cursor = self._connection.cursor()

    def trackcommand(self, channel, command):
        table = channel[1:]
        self.ensure_table_exists(table)
        self.increase_entry(table, command)

    def ensure_table_exists(self, table):
        self._cursor.execute("CREATE TABLE IF NOT EXISTS {} (command TEXT, count INTEGER)".format(table))

    def increase_entry(self, table, command):
        self._cursor.execute("SELECT * FROM {} WHERE command=?".format(table), (command,))
        entry = self._cursor.fetchone()
        if entry == None:
            self._cursor.execute("INSERT INTO {} VALUES (?, 1)".format(table), (command,))
        else:
            self._cursor.execute("UPDATE {} SET count=? WHERE command=?".format(table), (entry[1]+1, command))
        self._connection.commit()
