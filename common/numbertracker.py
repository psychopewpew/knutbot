import sqlite3
from common import genericdatabase

class NumberTracker():
    def __init__(self, tablename, filename="res/numbers.db"):
        self._filename = filename
        self.tablename = tablename
        self.db = genericdatabase.GenericDatabase(self._filename, tablename, [['name', 'TEXT', 'PRIMARY KEY'], ['number', 'INTEGER', ""]])

    @property
    def _cursor(self):
        return self.db.cursor

    @property
    def _connection(self):
        return self.db.connection

    def add(self, name, number):
        return self._set(name, lambda c: c+number)

    def clear(self):
        self._cursor.execute("DELETE FROM {}".format(self. tablename))
        self._connection.commit()

    def get(self, name):
        self._cursor.execute("SELECT * FROM {} WHERE lower(name)=lower(?)".format(self.tablename),(name,))
        result = self._cursor.fetchone()
        return result if result else (name, 0)
        
    def set(self, name, number):
        return self._set(name, lambda c: number)
        
    def _set(self, name, number_f):
        self._cursor.execute("SELECT * FROM {} WHERE lower(name)=lower(?)".format(self.tablename), (name,))
        current = self._cursor.fetchone()
        if current == None:
            number = number_f(0)
            self._cursor.execute("INSERT INTO {} VALUES (?, ?)".format(self.tablename), (name, number))
            self._connection.commit()
            return name, 0, number
        else:
            number = number_f(current[1])
            self._cursor.execute("UPDATE {} SET number=? WHERE lower(name)=lower(?)".format(self.tablename), (number, name))
            self._connection.commit()
            return current[0], current[1], number
            
    def sum(self):
        self._cursor.execute("SELECT SUM(number) FROM {}".format(self.tablename))
        result = self._cursor.fetchone()[0]
        return result if result else 0
