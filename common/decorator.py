from inspect import signature, stack, getmodule
from common import permissions, config, cooldown, commandtracker
from common.utils import wrap_and_change_color, ensure_dir_exists
import logging
from logging.handlers import RotatingFileHandler
import functools
import sys

def init():
    LOGDIR = "logs/"
    ensure_dir_exists(LOGDIR)
    global errorlogger
    errorlogger = logging.getLogger("error")
    h = RotatingFileHandler(LOGDIR+"error.log","a",3000000, 10)
    h.setFormatter(logging.Formatter('[%(asctime)s] %(message)s'))
    errorlogger.addHandler(h)
    h = logging.StreamHandler(stream=sys.stderr)
    h.setFormatter(logging.Formatter('[%(asctime)s] %(message)s'))
    errorlogger.addHandler(h)
    global cmdtracker
    cmdtracker = commandtracker.CommandTracker()

def returnNone(*args, **kwargs):
    return None

def max_userlevel(user):
    return max(user.permission, 
        permissions.MOD if user.mod else permissions.EVERYONE,
        permissions.VIP if user.vip else permissions.EVERYONE)

def verify_paramlist(f):
    params = signature(f).parameters
    paramlist = list(params.items())
    if paramlist[0][0] != "channel" or paramlist[1][0] != "user" or paramlist[2][0] != "args":
        raise Exception("Command does not follow definition (channel, user, args)")
    return paramlist


class Command(object):
    def __init__(self,
        required_permission=permissions.EVERYONE,
        call_if_permission_insufficient=returnNone,
        call_if_on_cooldown=returnNone):

        self.req_perm =  required_permission
        self.call_perm_insufficient = call_if_permission_insufficient
        self.call_on_cooldown = call_if_on_cooldown

    def __call__(self, f):
        verify_paramlist(f)

        # Define function for checking cooldown and executing the command
        # this is common between the two decorated functions
        def execute(channel, user, args, userlevel, whisper):
            # Check if it is on cooldown for the user
            if cooldown.is_on_cooldown(f.__name__, user, channel, userlevel=userlevel):
                return wrap_and_change_color(channel, self.call_on_cooldown(channel, user, args), whisper)
            try:
                if config.log_command_stats:
                    cmdtracker.trackcommand(channel, f.__name__)
                return wrap_and_change_color(channel, f(channel, user, args), whisper)
            except:
                errorlogger.exception("Error executing command '{}{} {}'.".format(
                    config.cmdprefix, f.__name__, ' '.join(args)))
                return wrap_and_change_color(channel, "Error executing command {}{}.".format(config.cmdprefix,
                    f.__name__), whisper)

        # Create decorated_f, depending on required permission level
        # If required permission is EVERYONE we don't need to check permission
        if self.req_perm == permissions.EVERYONE:
            def decorated_f(channel, user, args, whisper):
                userlevel = max_userlevel(user)
                return execute(channel, user, args, userlevel, whisper)
        else:
            def decorated_f(channel, user, args, whisper):
                userlevel = max_userlevel(user)
                # Check if userlevel is above required level
                if self.req_perm > userlevel:
                    return wrap_and_change_color(channel, self.call_perm_insufficient(channel, user, args), whisper)
                return execute(channel, user, args, userlevel, whisper)

        functools.update_wrapper(decorated_f, f)

        # Add command to module list of commands
        mod = sys.modules[f.__module__]
        if not hasattr(mod, "commandlist"):
            mod.commandlist = []
        mod.commandlist.append(f.__name__)

        decorated_f.required_permission = self.req_perm

        return decorated_f
        
class CustomCommand(object):
    def __init__(self, functionname, 
        required_permission=permissions.EVERYONE,
        call_if_on_cooldown=returnNone):
        self.functionname = functionname
        self.req_perm =  required_permission
        self.call_on_cooldown = call_if_on_cooldown
        
    def __call__(self, f):
        verify_paramlist(f)

        # Define function for checking cooldown and executing the command
        # this is common between the two decorated functions
        def execute(channel, user, args, userlevel, whisper):
            # Check if it is on cooldown for the user
            if cooldown.is_on_cooldown(self.functionname, user, channel, userlevel=userlevel):
                return wrap_and_change_color(channel, self.call_on_cooldown(channel, user, args), whisper)
            try:
                if config.log_command_stats:
                    cmdtracker.trackcommand(channel, self.functionname)
                return wrap_and_change_color(channel, f(channel, user, args), whisper)
            except:
                errorlogger.exception("Error executing command '{}{} {}'.".format(
                    config.cmdprefix, self.functionname, ' '.join(args)))
                return wrap_and_change_color(channel, "Error executing command {}{}.".format(config.cmdprefix,
                    self.functionname), whisper)

        # Create decorated_f, depending on required permission level
        # If required permission is EVERYONE we don't need to check permission
        if self.req_perm == permissions.EVERYONE:
            def decorated_f(channel, user, args, whisper):
                userlevel = max_userlevel(user)
                return execute(channel, user, args, userlevel, whisper)
        else:
            def decorated_f(channel, user, args, whisper):
                userlevel = max_userlevel(user)
                # Check if userlevel is above required level
                if self.req_perm > userlevel:
                    return wrap_and_change_color(channel, None, whisper)
                return execute(channel, user, args, userlevel, whisper)

        functools.update_wrapper(decorated_f, f)
        decorated_f.required_permission = self.req_perm

        return decorated_f

init()
