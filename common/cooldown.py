from common.config import Config, exit_event
import time
import threading
from common import permissions, utils

cooldowns = Config("res/cooldowns.json")
_users = {}

def is_on_cooldown(command, user, channel, userlevel=permissions.EVERYONE, renew_cd=True):
    if cooldowns.has(channel, command):
        cd = cooldowns.get(channel, command)
        if (not "ignoremod" in cd or cd["ignoremod"]) and userlevel >= permissions.MOD or userlevel >= permissions.OWNER:
            return False
        if cd["type"]  == "user":
            cdtime = cd["cooldown"]
            currenttime = time.time()

            # Make sure user with command exists in dict
            if channel not in _users:
                _users[channel] = {}
            if command not in _users[channel]:
                _users[channel][command] = {}
            if user not in _users[channel][command]:
                _users[channel][command][user] = 0

            # Check
            if _users[channel][command][user] + cdtime < currenttime:
                if renew_cd:
                    _users[channel][command][user] = currenttime
                return False
            else:
                return True
        elif cd["type"] == "global":
            cdtime = cd["cooldown"]
            currenttime = time.time()
            # Make sure command exists in dict
            if channel not in _users:
                _users[channel] = {}
            if command not in _users[channel]:
                _users[channel][command] = 0
            # Check
            if _users[channel][command] + cdtime < currenttime:
                if renew_cd:
                    _users[channel][command] = currenttime
                return False
            else:
                return True
    return False

def cleanup():
    currenttime = time.time()
    for channel in _users:
        for command in _users[channel]:
            cd = cooldowns.get(channel, command)
            if cd["type"] == "user":
                for user in _users[channel][command]:
                    if _users[channel][command][user] + cd["cooldown"] < currenttime:
                        del _users[channel][command][user]
                if not _users[channel][command]:
                    del _users[channel][command]
            else:
                if _users[channel][command] + cd["cooldown"] < currenttime:
                    del _users[channel][command]


def _cleanup_loop():
    while not exit_event.is_set():
        cleanup()
        utils.waitonevent(exit_event, 1, 3600)

def start_cleanup_loop():
    threading.Thread(target=_cleanup_loop).start()
