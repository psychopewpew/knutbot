from common.genericdatabase import GenericDatabase
import random

class GenericTextlist(GenericDatabase):
    
    """
    A generic textlist, that takes the filename, tablename and 
    additional columns. The first columns are always 
        * id INTEGER PRIMARY KEY
        * text TEXT
    """
    
    @staticmethod
    def get_full_columns(additionalcolumns):
        basecolumns = [["id","INTEGER","PRIMARY KEY"], ["text", "TEXT"]]
        if not additionalcolumns:
            return basecolumns
        else:
            return basecolumns + additionalcolumns
    
    def __init__(self, filename, tablename, additionalcolumns):
        super().__init__(filename, tablename, 
        self.get_full_columns(additionalcolumns))
        self._rand_used = []
        self._rand_avail = self.ids()
        
    def add(self, text, additionalvalues, commit=True):
        existmask = (False, True) + (False,) * (self.columnlen - 2)
        values = (None, text) + additionalvalues
        if super().add(existmask, values, commit):
            newid = self.maxid()
            self._rand_avail.append(newid)
            return newid
        else:
            return -1
        
    def delete(self, idx):
        deletemask = (True,) + (False,) * (self.columnlen - 1)
        deletevalue = (idx,) + (None,) * (self.columnlen - 1)
        return super().delete(deletemask, deletevalue)
        
    def edit(self, idx, newmask, newvalues):
        editmask = (True,) + (False,) * (self.columnlen - 1)
        editvalue = (idx,) + (None,) * (self.columnlen - 1)
        return super().edit(editmask, editvalue, newmask, newvalues)
        
    def maxid(self):
        self.cursor.execute("SELECT MAX(id) FROM {}".format(self._tablename))
        return self.cursor.fetchone()[0]

    def ids(self):
        self.cursor.execute("SELECT id FROM {}".format(self._tablename))
        return [r[0] for r in self.cursor.fetchall()]
        
    def getEntry(self, entryid=-1, blacklist=set()):
        entry = None
        if entryid == -1:
            # Filter blacklisted entries
            available = [e for e in self._rand_avail if e not in blacklist]
            # Empty list
            if not available:
                return None

            eid = random.choice(available)
            self._rand_avail.remove(eid)
            self._rand_used.append(eid)
            # Always keep available list half full
            if len(self._rand_used) > len(self._rand_avail):
                backavail = self._rand_used[0]
                self._rand_used = self._rand_used[1:]
                self._rand_avail.append(backavail)
            return self.getEntry(eid)
        else:
            self.cursor.execute("SELECT * FROM {} WHERE id=?".format(self._tablename),(entryid,))
        entry = self.cursor.fetchone()
        return entry

class SimpleTextlist(GenericTextlist):
    
    FILENAME = "res/simpletexts.db"
    
    def __init__(self, tablename, filename=FILENAME):
        super().__init__(filename, tablename, 
        None)
        
    def addEntry(self, text, commit=True):
        return self.add(text, (), commit)
        
    def deleteEntry(self, idx):
        return self.delete(idx)
        
    def editEntry(self, idx, newtext):
        return self.edit(idx, (False, True), (None, newtext))
        
    def getMaxId(self):
        return self.maxid()
        
    def getIds(self):
        return self.ids()
        
    def loadEntries(self, filename):
        try:
            with open(filename,"r") as f:
                ls = [l.strip() for l in f.readlines()]
                n = 0
                for l in ls:
                    if self.addEntry(l, commit=False) != -1:
                        n += 1
                self._connection.commit()
                return n
        except:
            raise
