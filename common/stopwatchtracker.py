import sqlite3
import time
from collections import namedtuple

class StopwatchTracker():

    _FILENAME = "res/stopwatchtracker.db"
    CalcResult = namedtuple('Result', ['value', 'count'])

    def __init__(self, tablename, channel):
        self._tablename = tablename
        self._connection = sqlite3.connect(self._FILENAME)
        self._cursor = self._connection.cursor()

        self.starttime = {}
        self._channelowner = channel[1:]

        self._cursor.execute("CREATE TABLE IF NOT EXISTS {} (id INTEGER PRIMARY KEY, time_in_seconds REAL, timestamp INTEGER)".format(self._tablename))

    def start(self, key="default"):
        if key in self.starttime:
            return False
        self.starttime[key] = time.time()
        return True

    def stop(self, key="default"):
        if key not in self.starttime:
            return -1
        stoptime = time.time()
        diff = stoptime - self.starttime[key]
        del self.starttime[key]
        self._cursor.execute("INSERT INTO {} VALUES (?, ?, ?)".format(self._tablename), (None, diff, stoptime))
        self._connection.commit()
        return diff

    def calculate(self, function, start, stop):
        self._cursor.execute("SELECT {}(time_in_seconds), COUNT(*) FROM {} WHERE {} <= timestamp AND timestamp <=  {}".format(function, self._tablename, start, stop))
        tmp = self._cursor.fetchone()
        return self.CalcResult(value=tmp[0], count=tmp[1])

