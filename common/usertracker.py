import sqlite3
from common.utils import strtobool
from common import permissions, genericdatabase

_trackers = {}


def gettracker(channel):
    if channel not in _trackers:
        _trackers[channel] = UserTracker(channel)
    return _trackers[channel]


class UserTracker(object):

    _FILENAME = "res/users.db"

    def __init__(self, channel):
        self._users = {}
        self._tablename = channel[1:]
        self._db = genericdatabase.GenericDatabase(
            self._FILENAME,
            self._tablename,
            [["username", "TEXT", "PRIMARY KEY"],
             ["displayname", "TEXT"],
             ["mod", "INTEGER"],
             ["subscriber", "INTEGER"],
             ["color", "TEXT"],
             ["permission", "INTEGER"],
             ["vip", "INTEGER"],
             ])
        results = self._db.getall()
        for result in results:
            self._users[result[0]] = User(*result)

    def empty(self):
        return not self._users

    def hasowner(self):
        return self._tablename in self._users

    def insertdb(self, user):
        self._db.add((True,) + (False,) * 6,
                     (user.name, user.displayname, user.mod,
                      user.subscriber, user.color, user.permission.level,
                      user.vip))

    def update_permission(self, user, permission):
        if type(user) == str:
            user = self.get(user)
        if user.name == self._tablename and permission < permissions.OWNER:
            return
        if user.permission != permission:
            user.permission = permission
            self.updatedb(user)

    def get(self, username):
        return self.verify_user_exists(username)

    def getmods(self):
        return [u for u in self._users.values() if u.permission == permissions.MOD]

    def getowners(self):
        return [u for u in self._users.values() if u.permission >= permissions.OWNER]

    def getvips(self):
        return [u for u in self._users.values() if u.permission == permissions.VIP]

    def getall(self):
        return self._users.values()

    def update_and_get(self, username, tags):
        user = self.verify_user_exists(username)
        self.update_with_tags(user, tags)
        return user

    def update_with_tags(self, user, tags):
        changed = False
        for tag in tags:
            k,v = tag["key"], tag["value"]
            if k == "color" and user.color != v:
                user.color = v
                changed = True
            elif k == "display-name" and user.displayname != v:
                user.displayname = v
                changed = True
            elif k == "mod":
                b = strtobool(v)
                if user.mod != b:
                    user.mod = b
                    changed = True
            elif k == "subscriber":
                b = strtobool(v)
                if user.subscriber != b:
                    user.subscriber = b
                    changed = True
            elif k == "badges" and v != None:
                b = "vip" in v
                if user.vip != b:
                    user.vip = b
                    changed = True
        if changed:
            self.updatedb(user)

    def updatedb(self, user):
        self._db.edit((True,) + (False,) * 6,
                      (user.name,)+(None,) * 6,
                      (False,) + (True,) * 6,
                      (None, user.displayname, user.mod, user.subscriber, user.color,
                       user.permission.level, user.vip))
        print("Updating user in {}: {}".format(self._tablename, repr(user)))

    def verify_user_exists(self, username):
        username = username.lower()[1:] if username.startswith("@") else username.lower()
        if username not in self._users:
            u = User(username)
            self._users[username] = u
            self.insertdb(u)
        return self._users[username]


class User(object):

    def __init__(self, name, displayname=None, mod=False, subscriber=False,
                 color=None, permission=permissions.EVERYONE, vip=False):

        self.name = name
        self.displayname = displayname if displayname else name
        self.mod = bool(mod)
        self.subscriber = bool(subscriber)
        self.color = color
        self.vip = bool(vip)
        self.permission = permission if type(permission) == permissions.Permissionlevel else permissions.permdict[permission]

    def __str__(self):
        return self.displayname

    def __repr__(self):
        return "User(name={}, displayname={}, mod={}, subscriber={}, color={}, permission={}, vip={})".format(
            self.name, self.displayname, self.mod, self.subscriber, self.color,
            self.permission, self.vip)
    
    def to_json(self):
        d = dict(self.__dict__)
        d['permission'] = str(d['permission'])
        return d
