from abc import ABC, abstractmethod
from collections import namedtuple
from common.config import moduleconfig

class RecentRequestError(Exception):
    def __init__(self, entry):
        self.entry = entry

class CurrentRequestError(Exception):
    def __init__(self, entry):
        self.entry = entry

class UpcomingRequestError(Exception):
    
    def __init__(self, entry, current_position):
        self.entry = entry
        self.current_position = current_position

class QueueItem(object):
    
    def __init__(self, user, entry, pushback = 0):
        self.user = user
        self.entry = entry
        self.pushback = pushback
        
    def __repr__(self):
        return "QueueItem(user='{}', entry='{}', pushback={})".format(
            self.user, self.entry, self.pushback)

class ScheduledQueue(ABC):
    
    def __init__(self, recent_limit=0, module_setting_path=None):
        self._recent = []
        self.current = None
        self._queue = []
        self._recent_limit = recent_limit
        self._module_setting_path = module_setting_path
        self.load()
    
    @abstractmethod
    def add(self, user, entry):
        pass
        
    def clear(self):
        self._recent = []
        self.current = None
        self._queue = []
        self.save()
    
    def checkQueue(self, entry):
        if any([i.entry == entry for i in self._recent]):
            raise RecentRequestError(entry)
        if self.current and self.current.entry == entry:
            raise CurrentRequestError(entry)
        if any([i.entry == entry for i in self._queue]):
            raise UpcomingRequestError(
                entry, 
                [i.entry for i in self._queue].index(entry)
                )
    
    def forward(self):
        if self.current:
            self._recent.append(self.current)
            self._recent[:] = self._recent[-self.recent_limit:]
        self.current = self._queue[0] if self._queue else None
        self._queue[:] = self._queue[1:]
        self.save()
        return bool(self.current)
        
    def load(self):
        if self._module_setting_path:
            if moduleconfig.has(*self._module_setting_path):
                recentlist_repr = moduleconfig.get_default([], *self._module_setting_path, "recent")
                self._recent = list(map(eval, recentlist_repr))
                self.current = eval(moduleconfig.get_default("None", *self._module_setting_path, "current"))
                queue_repr = moduleconfig.get_default([], *self._module_setting_path, "queue")
                self._queue = list(map(eval, queue_repr))
        
    @property
    def recent_limit(self):
        return self._recent_limit
    
    @recent_limit.setter
    def recent_limit(self, limit):
        self._recent_limit = limit
        self._recent[:] = self._recent[-self.recent_limit:]
        self.save()
        
    def save(self):
        if self._module_setting_path:
            recentlist = list(map(repr, self._recent))
            moduleconfig.set(recentlist, *self._module_setting_path, "recent")
            moduleconfig.set(repr(self.current), *self._module_setting_path, "current")
            queuelist = list(map(repr, self._queue))
            moduleconfig.set(queuelist, *self._module_setting_path, "queue")
        
    def upcoming(self, number=3):
        return self._queue[:number]


class PrimitiveQueue(ScheduledQueue):
    
    def add(self, user, entry):
        self.checkQueue(entry)
        self._queue.append(QueueItem(user, entry))
        self.save()
        return len(self._queue)

class UserBasedQueue(ScheduledQueue):
    
    MAXIMUM_PUSHBACK = 3
    
    def add(self, user, entry):
        position = self._add(user, entry)
        self.save()
        return position
        
    def _add(self, user, entry):
        self.checkQueue(entry)
        newitem = QueueItem(user, entry)
        fulllist = self._recent + ([self.current] if self.current else []) + self._queue
        if not self._queue:
            self._queue.append(newitem)
            return 1
        
        distancelist = [0]*len(fulllist)
        lastseen = {}
        for i in range(len(distancelist)):
            item = fulllist[i]
            if item.user in lastseen:
                distancelist[i] = i-lastseen[item.user]
            lastseen[item.user] = i
        
        newdistance = len(distancelist)-lastseen[newitem.user] if user in lastseen else 0
        
        # Crop recent and current from list because they cannot be changed
        distancelist[:] = distancelist[len(self._recent) + (
            1 if self.current else 0):]
        
        
        if newdistance == 0:
            # New user, push forward till other new user is reached 
            # or maximum number of pushbacks
            for i in range(len(distancelist)-1, -1, -1):
                if self._queue[i].pushback == self.MAXIMUM_PUSHBACK:
                    self._queue.insert(i+1, newitem)
                    return i+1
                elif distancelist[i] != 0:
                    self._queue[i].pushback += 1
                else:
                    self._queue.insert(i+1, newitem)
                    return i+1
            self._queue.insert(0, newitem)
            return 1
        else:
            # Old user, try to push forward till distance or
            # pushback limit reached
            for i in range(len(distancelist)-1, -1, -1):
                if self._queue[i].pushback == self.MAXIMUM_PUSHBACK:
                    self._queue.insert(i+1, newitem)
                    return i+1
                elif distancelist[i] == 0:
                    self._queue.insert(i+1, newitem)
                    return i+1
                elif distancelist[i] + 1 <= newdistance - 1:
                    newdistance -= 1
                    self._queue[i].pushback += 1
                else:
                    self._queue.insert(i+1, newitem)
                    return i+1
            self._queue.insert(0, newitem)
            return 1
    

class SubUserBasedQueue(ScheduledQueue):
    
    MAXIMUM_PUSHBACK = 3
    
    def add(self, user, entry):
        position = self._add(user, entry)
        self.save()
        return position
        
    def _add(self, user, entry):
        self.checkQueue(entry)
        newitem = QueueItem(user, entry)
        fulllist = self._recent + ([self.current] if self.current else []) + self._queue
        if not self._queue:
            self._queue.append(newitem)
            return 1
        
        distancelist = [0]*len(fulllist)
        lastseen = {}
        for i in range(len(distancelist)):
            item = fulllist[i]
            if item.user in lastseen:
                distancelist[i] = i-lastseen[item.user]
            lastseen[item.user] = i
        
        newdistance = len(distancelist)-lastseen[newitem.user] if user in lastseen else 0
        
        # Crop recent and current from list because they cannot be changed
        distancelist[:] = distancelist[len(self._recent) + (
            1 if self.current else 0):]
        
        
        if newdistance == 0:
            # New user, push forward till other new user is reached 
            # or maximum number of pushbacks
            for i in range(len(distancelist)-1, -1, -1):
                if self._queue[i].pushback == self.MAXIMUM_PUSHBACK:
                    self._queue.insert(i+1, newitem)
                    return i+1
                elif distancelist[i] != 0:
                    self._queue[i].pushback += 1
                elif newitem.user.subscriber and not self._queue[i].user.subscriber:
                    self._queue[i].pushback += 1
                else:
                    self._queue.insert(i+1, newitem)
                    return i+1
            self._queue.insert(0, newitem)
            return 1
        else:
            # Old user, try to push forward till distance or
            # pushback limit reached
            for i in range(len(distancelist)-1, -1, -1):
                if self._queue[i].pushback == self.MAXIMUM_PUSHBACK:
                    self._queue.insert(i+1, newitem)
                    return i+1
                elif distancelist[i] == 0:
                    self._queue.insert(i+1, newitem)
                    return i+1
                elif distancelist[i] + 1 <= newdistance - 1:
                    newdistance -= 1
                    self._queue[i].pushback += 1
                elif newitem.user.subscriber and not self._queue[i].user.subscriber \
                    and distancelist[i] <= newdistance:
                    newdistance -= 1
                    self._queue[i].pushback += 1
                else:
                    self._queue.insert(i+1, newitem)
                    return i+1
            self._queue.insert(0, newitem)
            return 1

def test():
    from common import usertracker
    tracker = usertracker.UserTracker("#psychopewpew")
    
    q = UserBasedQueue(3)
    q.add(tracker.get("PsychoPewPew"), 1) # 1
    q.add(tracker.get("Xillians"), 2) # 2
    q.add(tracker.get("PsychoPewPew"), 3) # 6
    q.add(tracker.get("Trinidad_Jared"), 4) # 3
    q.add(tracker.get("Xillians"), 5) # 8
    q.add(tracker.get("TwanM4n"), 6) # 4
    #print("Forwarding..")
    #q.forward()
    #q.forward()
    q.add(tracker.get("mhientaye"), 7) # 5
    q.add(tracker.get("Trinidad_Jared"), 8) # 9
    q.add(tracker.get("Xillians"), 9) # 12
    q.add(tracker.get("TwanM4n"), 10) # 10
    q.add(tracker.get("Jibberdoo"), 11) # 7
    q.add(tracker.get("mhientaye"), 12) # 11
    print(q._queue)
    
    q = SubUserBasedQueue(3)
    q.add(tracker.get("PsychoPewPew"), 1) # 1
    q.add(tracker.get("Xillians"), 2) # 3
    q.add(tracker.get("PsychoPewPew"), 3) # 6
    q.add(tracker.get("Trinidad_Jared"), 4) # 4
    q.add(tracker.get("Xillians"), 5) # 8
    q.add(tracker.get("TwanM4n"), 6) # 5
    #print("Forwarding..")
    #q.forward()
    #q.forward()
    q.add(tracker.get("mhientaye"), 7) # 2
    q.add(tracker.get("Trinidad_Jared"), 8) # 10
    q.add(tracker.get("Xillians"), 9) # 12
    q.add(tracker.get("TwanM4n"), 10) # 11
    q.add(tracker.get("Jibberdoo"), 11) # 7
    q.add(tracker.get("mhientaye"), 12) # 9
    print(q._queue)
