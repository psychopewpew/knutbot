import sqlite3
from collections import namedtuple

class TimeTracker():

    _FILENAME = "res/timetracker.db"
    CalcResult = namedtuple('Result', ['value', 'count'])

    def __init__(self, tablename):
        self._tablename = tablename
        self._connection = sqlite3.connect(self._FILENAME)
        self._cursor = self._connection.cursor()

        self._cursor.execute("CREATE TABLE IF NOT EXISTS {} (entry TEXT PRIMARY KEY, timestamp INTEGER)".format(self._tablename))

    def update(self, entry, time):
        if self.get_timestamp(entry) != None:
            self._cursor.execute("UPDATE {} SET timestamp=? WHERE entry=?".format(self._tablename),
                (time, entry))
        else:
            self._cursor.execute("INSERT INTO {} VALUES (?, ?)".format(self._tablename),
                (entry, time))
        self._connection.commit()
        return True

    def get_timestamp(self, entry):
        self._cursor.execute("SELECT timestamp FROM {} WHERE entry=?".format(self._tablename), (entry,))
        r = self._cursor.fetchone()
        return r[0] if r else None
