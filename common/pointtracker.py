import sqlite3
import time

class PointTracker():

    def __init__(self, filename, current_table):
        self._filename = filename
        self._connection = sqlite3.connect(self._filename)
        self._cursor = self._connection.cursor()

        self.current_table = current_table

        self._cursor.execute("CREATE TABLE IF NOT EXISTS players (id INTEGER PRIMARY KEY, name TEXT)")

    def addPlayer(self, name):
        if self.playerExists(name):
            return False
        self._cursor.execute("INSERT INTO players VALUES (?,?)",(None, name))
        self._connection.commit()
        return True

    def currentTableExists(self):
        try:
            self._cursor.execute("SELECT * FROM {} LIMIT 1".format(self.current_table))
            self._cursor.fetchall()
            return True
        except:
            return False

    def deletePlayer(self, name):
        if not self.playerExists(name):
            return False
        self._cursor.execute("DELETE FROM players WHERE name=lower(?)", (name,))
        self._connection.commit()
        return True

    def renamePlayer(self, oldname, newname):
        player = self.getPlayer(oldname)
        if not player:
            return False
        if oldname.lower() != newname.lower():
            newplayer = self.getPlayer(newname)
            if newplayer:
                return False
        self._cursor.execute("UPDATE players SET name=? WHERE id=?", (newname, player[0]))
        self._connection.commit()
        return True

    def playerExists(self, name):
        return self.getPlayer(name) != None

    def getPlayer(self, name):
        self._cursor.execute("SELECT * FROM players WHERE lower(name)=lower(?)",(name,))
        return self._cursor.fetchone()

    def addPoints(self, player, points):
        idx = player[0]
        self._cursor.execute("CREATE TABLE IF NOT EXISTS {} (id INTEGER PRIMARY KEY, points INTEGER)".format(self.current_table))

        self._cursor.execute("SELECT * FROM {} WHERE id=?".format(self.current_table), (idx,))
        current = self._cursor.fetchone()
        if current == None:
            self._cursor.execute("INSERT INTO {} VALUES (?, ?)".format(self.current_table), (idx, points))
            self._connection.commit()
            return (player[1], 0, points)
        else:
            self._cursor.execute("UPDATE {} SET points=? WHERE id=?".format(self.current_table), (current[1] + points, idx))
            self._connection.commit()
            return (player[1], current[1], current[1]+points)

    def getStandings(self, tablename=None):
        if tablename == None:
            tablename =  self.current_table
        if self.currentTableExists():
            self._cursor.execute("SELECT name, points FROM players AS p, {} AS curp WHERE p.id = curp.id".format(tablename))
            return self._cursor.fetchall()
        else:
            return None


class MonthlyPointTracker(PointTracker):

    def __init__(self, filename):
        localtime = time.localtime()
        super().__init__(filename, "`{}-{}`".format(localtime.tm_year, localtime.tm_mon))

    def currentTableExists(self):
        self.updateTablename()
        try:
            self._cursor.execute("SELECT * FROM {} LIMIT 1".format(self.current_table))
            self._cursor.fetchall()
            return True
        except:
            return False

    def updateTablename(self):
        localtime = time.localtime()
        self.current_table = "`{}-{}`".format(localtime.tm_year, localtime.tm_mon)

    def addPoints(self, player, points):
        self.updateTablename()
        return super().addPoints(player, points)

    def getStandings(self, date=None):
        if date == None:
            localtime = time.localtime()
            tablename = "`{}-{}`".format(localtime.tm_year, localtime.tm_mon)
        else:
            tablename = "`{}-{}`".format(date["year"], date["month"])
        return super().getStandings(tablename)
