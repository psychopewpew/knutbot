from tornado_json.routes import get_routes
from tornado_json.application import Application
from tornado.ioloop import IOLoop
from modules import controller
import asyncio
import json


def remove_prefix(route):
    url = route[0][:-1]
    slash = url.rfind('/', 0, len(url)-2)
    return url[slash:], route[1]

def make_app():
    asyncio.set_event_loop(asyncio.new_event_loop())
    from . import handlers
    routes = list(map(remove_prefix, get_routes(handlers)))
    return Application(routes=routes, settings={}, generate_docs=True)


def startServer():
    app = make_app()
    app.listen(9000)
    IOLoop().instance().start()
