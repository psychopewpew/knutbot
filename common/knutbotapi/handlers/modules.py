from tornado_json import schema
from tornado_json.exceptions import APIError
from modules import controller, moduleloader
from common import usertracker, permissions
from common.knutbotapi.handlers.apihandler import KnutbotAPIHandler


class Modules(KnutbotAPIHandler):

    def initialize(self):
        super().initialize()
        self.check_channel = True

    @schema.validate(
        output_schema={
            "type": "object"
        },
        output_example={
            "#psychopewpew": ["moduleloader"]
        }
    )
    def get(self):
        if self.user.permission < permissions.OWNER:
            raise APIError(401, "You don't have the required permission.")
        active = controller._activated_modules[self.channel]
        modules = controller._modules
        result = {m: (m in active) for m in modules}
        return result

    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "module": {"type": "string"},
            }
        },
        input_example={
            "module": "channelcolor"
        }
    )
    def post(self):
        r = moduleloader.modules(self.channel,
                                 self.user,
                                 ["activate", self.body["module"]],
                                 False)
        return r

    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "module": {"type": "string"},
            }
        },
        input_example={
            "module": "channelcolor"
        }
    )
    def delete(self):
        r = moduleloader.modules(self.channel,
                                 self.user,
                                 ["deactivate", self.body["module"]],
                                 False)
        return r
