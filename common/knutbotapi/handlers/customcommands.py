from tornado_json import schema
from tornado_json.exceptions import APIError
from modules import controller, moduleloader
from common import usertracker, permissions
from common.knutbotapi.handlers.apihandler import KnutbotAPIHandler
from modules import customcommands
import json

class Customcommands(KnutbotAPIHandler):

    def initialize(self):
        super().initialize()
        self.check_channel = True

    @schema.validate(
        output_schema={
            "type": "array"
        },
        output_example=[["so", "Check out this person", "mod"]]
    )
    def get(self):
        commands = customcommands.getall(self.channel)
        try:
            cs = list(map(lambda c: (
                c[0],
                c[1],
                str(permissions.permdict[c[2]])),
                          commands))
            return sorted(cs, key=lambda c: c[0])
        except TypeError as e:
            raise APIError(503, "Custom Commands are not active.")

    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "command": {"type": "string"},
                "response": {"type": "string"},
                "permission": {"type": "string"},
            }
        },
        input_example={
            "command": "newcommand",
            "response": "test response",
            "permission": "Everyone"
        }
    )
    def post(self):
        command = self.body['command']
        if ' ' in command:
            raise APIError(400, "Command must be a single word")
        new_permission = self.body['permission']
        levelobject = getattr(permissions, new_permission.upper())

        args = [command] + self.body['response'].split()

        r = customcommands.addcommand(self.channel,
                                  self.user,
                                  args,
                                  True)[0]
        
        if r != "Added command":
            raise APIError(400, r)
        args = [command, str(levelobject)]
        customcommands.setcmdperm(self.channel,
                                  self.user,
                                  args,
                                  True
                                  )

    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "command": {"type": "string"},
                "response": {"type": "string"},
                "permission": {"type": "string"},
            }
        },
        input_example={
            "command": "newcommand",
            "response": "test response",
            "permission": "Everyone"
        }
    )
    def put(self):
        command = self.body['command']
        new_permission = self.body['permission']
        levelobject = getattr(permissions, new_permission.upper())

        args = [command] + self.body['response'].split()

        customcommands.editcommand(self.channel,
                                   self.user,
                                   args,
                                   True)
        args = [command, str(levelobject)]
        customcommands.setcmdperm(self.channel,
                                  self.user,
                                  args,
                                  True
                                  )

    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "command": {"type": "string"},
            }
        },
        input_example={
            "command": "oldcommand"
        }
    )
    def delete(self):
        command = self.body['command']
        customcommands.delcommand(self.channel,
                                  self.user,
                                  [command],
                                  True)
