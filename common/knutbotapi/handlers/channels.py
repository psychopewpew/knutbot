from tornado_json import schema
from tornado_json.exceptions import APIError
from common import usertracker, permissions, config
from common.knutbotapi.handlers.apihandler import KnutbotAPIHandler


class Channels(KnutbotAPIHandler):

    @schema.validate(
        output_schema={
            "type": "object"
        },
        output_example={
            "channels": ["#psychopewpew", "#psychobotbot"]
        }
    )
    def get(self):
        result = []
        for channel in config.get('channels'):
            user = usertracker.gettracker(channel).get(self.username)
            if user.permission >= permissions.MOD:
                result.append(channel)
        return {"channels": sorted(result)}
