from tornado_json.requesthandlers import APIHandler
from tornado_json.exceptions import APIError
from common import usertracker
from common.twitchapi import helper

class KnutbotAPIHandler(APIHandler):
    
    def initialize(self):
        super().initialize()
        self.check_channel = False
        
    
    def prepare(self):
        headers = self.request.headers
        if 'Authorization' not in headers or len(headers['Authorization']) < 25:
            raise APIError(401, "Must provide Twitch OAuth token in header")
        if self.check_channel:
            if 'Channel' not in headers:
                raise APIError(400, "Must provide the channel in header")
            self.channel = headers['channel']
        self.username = helper.username(headers['Authorization'].split()[1])
        if self.check_channel:
            self.user = usertracker.gettracker(self.channel).get(self.username)
