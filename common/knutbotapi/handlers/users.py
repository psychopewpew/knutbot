from tornado_json import schema
from tornado_json.exceptions import APIError
from common import usertracker, permissions
from common.knutbotapi.handlers.apihandler import KnutbotAPIHandler


class Users(KnutbotAPIHandler):

    def initialize(self):
        super().initialize()
        self.check_channel = True

    @schema.validate(
        output_schema={
            "type": "object"
        },
        output_example={
            "owner": ["PsychoPewPew"],
            "mod": ["PsychoBotBot"],
            "vip": [],
        }
    )
    def get(self):
        if self.user.permission < permissions.OWNER:
            raise APIError(401, "You don't have the required permission.")
        tracker = usertracker.gettracker(self.channel)
        special = self.get_argument("special", False, True)
        if special:
            result = {
                "owner": list(map(str, tracker.getowners())),
                "mod": list(map(str, tracker.getmods())),
                "vip": list(map(str, tracker.getvips())),
                }
            return result
        else:
            allusers = tracker.getall()
            return {u.name: u.to_json() for u in allusers}

    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "username": {"type": "string"},
                "permission": {"type": "string"},
            }
        },
        input_example={
            "username": "psychobotbot",
            "permission": "mod"
        }
    )
    def put(self):
        username = self.body['username']
        new_permission = self.body['permission']
        levelobject = getattr(permissions, new_permission.upper())
        tracker = usertracker.gettracker(self.channel)
        tracker.update_permission(username.lower(), levelobject)
