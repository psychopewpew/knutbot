from tornado_json import schema
from tornado_json.exceptions import APIError
from modules import controller, moduleloader
from common import usertracker, permissions
from common.knutbotapi.handlers.apihandler import KnutbotAPIHandler
from modules import alias
import json

class Alias(KnutbotAPIHandler):

    def initialize(self):
        super().initialize()
        self.check_channel = True

    @schema.validate(
        output_schema={
            "type": "array"
        },
        output_example=[["dsdeath", "!death Dark Souls"]]
    )
    def get(self):
        als = alias.getall(self.channel)
        try:
            return sorted(als, key=lambda a: a[0])
        except TypeError as e:
            raise APIError(503, "Aliases are not active.")

    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "alias": {"type": "string"},
                "response": {"type": "string"}
            }
        },
        input_example={
            "alias": "dsdeath",
            "response": "!death Dark Souls"
        }
    )
    def post(self):
        a = self.body['alias']
        args = [a] + self.body['response'].split()

        r = alias.addalias(self.channel,
                           self.user,
                           args,
                           True)[0]
        
        if r != "Added alias.":
            raise APIError(400, r)

    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "alias": {"type": "string"},
            }
        },
        input_example={
            "alias": "oldalias"
        }
    )
    def delete(self):
        a = self.body['alias']
        alias.delalias(self.channel,
                       self.user,
                       [a],
                       True)
