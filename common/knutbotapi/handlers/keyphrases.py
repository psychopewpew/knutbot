from tornado_json import schema
from tornado_json.exceptions import APIError
from modules import controller, moduleloader
from common import usertracker, permissions
from common.knutbotapi.handlers.apihandler import KnutbotAPIHandler
from modules import keyphrases
import json


class Keyphrases(KnutbotAPIHandler):

    def initialize(self):
        super().initialize()
        self.check_channel = True

    @schema.validate(
        output_schema={
            "type": "array"
        },
        output_example=[["pun", "!pun", "0"]]
    )
    def get(self):
        kps = keyphrases.getall(self.channel)
        try:
            return sorted(kps, key=lambda k: k[0])
        except TypeError as e:
            raise APIError(503, "Custom Commands are not active.")

    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "keyphrase": {"type": "string"},
                "response": {"type": "string"},
                "cooldown": {"type": "string"},
            }
        },
        input_example={
            "keyphrase": "new keyphrase",
            "response": "test response",
            "cooldown": "0"
        }
    )
    def post(self):
        keyphrase = self.body['keyphrase']
        if '"' in keyphrase:
            raise APIError(400, "Keyphrase cannot contain \"")
        try:
            int(self.body['cooldown'])
        except ValueError as e:
            raise APIError(400, "Cooldown needs to be an integer")
        keyphrase = ('"' + keyphrase + '"').split()
        args = keyphrase + self.body['response'].replace('\n', '\\n').split()

        r = keyphrases.addkeyphrase(self.channel,
                                    self.user,
                                    args,
                                    True)[0]
        
        if not r.startswith("Added keyphrase"):
            raise APIError(400, r)
        args = keyphrase + [self.body['cooldown']]
        r = keyphrases.cdkeyphrase(self.channel,
                                   self.user,
                                   args,
                                   True
                                   )[0]
        if not r.startswith("Set cooldown for"):
            raise APIError(400, r)

    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "keyphrase": {"type": "string"},
                "response": {"type": "string"},
                "cooldown": {"type": "string"},
            }
        },
        input_example={
            "keyphrase": "new keyphrase",
            "response": "test response",
            "cooldown": "0"
        }
    )
    def put(self):
        keyphrase = self.body['keyphrase']
        if '"' in keyphrase:
            raise APIError(400, "Keyphrase cannot contain \"")
        try:
            int(self.body['cooldown'])
        except ValueError as e:
            raise APIError(400, "Cooldown needs to be an integer")
        keyphrase = ('"' + keyphrase + '"').split()
        args = keyphrase + self.body['response'].replace('\n', '\\n').split()
        r = keyphrases.editkeyphrase(self.channel,
                                     self.user,
                                     args,
                                     True)[0]

        if not r.startswith("Keyphrase '"):
            raise APIError(400, r)
        args = keyphrase + [self.body['cooldown']]
        r = keyphrases.cdkeyphrase(self.channel,
                                   self.user,
                                   args,
                                   True
                                   )[0]
        if not r.startswith("Set cooldown for"):
            raise APIError(400, r)

    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "command": {"type": "string"},
            }
        },
        input_example={
            "command": "oldcommand"
        }
    )
    def delete(self):
        keyphrase = self.body['keyphrase']
        keyphrases.delkeyphrase(self.channel,
                                self.user,
                                [keyphrase],
                                True)[0]
