from tornado_json import schema
from tornado_json.exceptions import APIError
from modules import controller, moduleloader
from common import usertracker, permissions
from common.knutbotapi.handlers.apihandler import KnutbotAPIHandler
from modules import customcounters
import json

class Customcounter(KnutbotAPIHandler):

    def initialize(self):
        super().initialize()
        self.check_channel = True

    @schema.validate(
        output_schema={
            "type": "array"
        },
        output_example=[["death", "died"]]
    )
    def get(self):
        counters = customcounters.getall(self.channel)
        try:
            return sorted(counters, key=lambda c: c[0])
        except TypeError as e:
            raise APIError(503, "Custom Commands are not active.")

    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "counter": {"type": "string"},
                "verb": {"type": "string"}
            }
        },
        input_example={
            "counter": "death",
            "verb": "died"
        }
    )
    def post(self):
        counter = self.body['counter']
        args = [counter] + self.body['verb'].split()

        r = customcounters.addcustomcounter(self.channel,
                                            self.user,
                                            args,
                                            True)[0]
        
        if not r.startswith("Added custom counter."):
            raise APIError(400, r)

    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "counter": {"type": "string"},
            }
        },
        input_example={
            "counter": "oldcounter"
        }
    )
    def delete(self):
        counter = self.body['counter']
        customcounters.delcustomcounter(self.channel,
                                  self.user,
                                  [counter],
                                  True)
