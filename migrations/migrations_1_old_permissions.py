from common import config, usertracker

def migrate():
    old_permission = config.get_default(None, 'permissions')
    if old_permission:
        for channel in old_permission:
            permissionlist = old_permission[channel]
            for user in permissionlist:
                utracker.update_permission(user, permissions.permdict[permissionlist[user]])
            config.delete('permissions',channel)
