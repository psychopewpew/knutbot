import sqlite3
from common.usertracker import UserTracker

def migrate():
    connection = sqlite3.connect(UserTracker._FILENAME)
    cursor = connection.cursor()
    cursor.execute("PRAGMA user_version")
    version = cursor.fetchone()[0]
    if version < 1:
        cursor.execute("SELECT name FROM sqlite_master where type='table'")
        tables = list(map(lambda e: e[0], cursor.fetchall()))
        for table in tables:
            cursor.execute("ALTER TABLE {} ADD COLUMN vip INTEGER DEFAULT 0".format(table))
        cursor.execute("PRAGMA user_version=1")
        connection.commit()
