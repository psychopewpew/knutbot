import sqlite3
import modules.customrequests

def migrate():
    connection = sqlite3.connect(modules.customrequests.DATABASE_FILENAME)
    cursor = connection.cursor()
    cursor.execute("PRAGMA user_version")
    version = cursor.fetchone()[0]
    print("Mig 4")
    if version < 1:
        cursor.execute("SELECT name FROM sqlite_master where type='table'")
        tables = list(map(lambda e: e[0], cursor.fetchall()))
        for table in tables:
            print(table)
            if table.startswith("req_"):
                continue
            cursor.execute("ALTER TABLE {} ADD COLUMN queue_type TEXT DEFAULT 'PrimitiveQueue'".format(table))
        cursor.execute("PRAGMA user_version=1")
        connection.commit()
