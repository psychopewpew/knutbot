from common import config

def migrate():
    channel_modules = config.get_default([], 'modules')
    for channel in channel_modules:
        modules = set(channel_modules[channel])
        death = "deathcounter" in modules
        confused = "confusedcounter" in modules
        if death or confused:
            modules.add("customcounters")
            from modules import customcounters
            from common import genericdatabase
            db = genericdatabase.GenericDatabase(
                customcounters.DATABASE_FILENAME, channel[1:], 
                [["name", "TEXT", "PRIMARY KEY"], 
                ["verb", "TEXT"]])
            if death:
                modules.remove("deathcounter")
                db.add((True, False), ("death", "died"))
            if confused:
                modules.remove("confusedcounter")
                db.add((True, False), ("confused", "was confused"))
            config.set(list(modules), "modules", channel)
